# Source Files For Protocol Buffer

This directory contains the source `uio_message.proto` file, which defines the attributes of the UIOMessage data format
being exchanged between the client and the server. The `.proto` file can be used to generate the `UIOMessage` class in Python3
and C++, which is used by this software. The steps for the compilation is explained below.  

## Generating Message Files

First, `protobuf-devel` package must be installed, which installs the protocol buffer runtime library (`protobuf`), and protocol buffer compiler (`protoc`).

```bash
# Install package
yum install protobuf-devel

# Check protobuf compiler version
protoc --version
```

### Using Makefile

One can use the `make` command to generate both Python and C++ message files.

```bash
# Check compiler version
make check_version

# Compile the .proto files into Python and C++
make
```

Alternatively, one can use `protoc` directly for Python and C++ separately, as explained below.

### Python

To generate the message class in Python (i.e. `uio_message_obj.py` here), you can use:

```bash
protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/uio_message.proto

# For example:
protoc -I=./ --python_out=./ uio_message.proto
```

### C++

The method to generate message class file for C++ is very similar to the Python case, only difference is we need the `--cpp_out` flag instead:

```bash
protoc -I=$SRC_DIR --cpp_out=$DST_DIR $SRC_DIR/uio_message.proto

# For example:
protoc -I=./ --cpp_out=./ ./uio_message.proto
```