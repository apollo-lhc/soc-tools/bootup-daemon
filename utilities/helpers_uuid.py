# 
# Generic helper functions.
# 

from .definitions import LOADED_UIOS_PER_UID

def get_uuid_from_message(incoming_message) -> str:
  """
  Returns the UUID from the incoming client message.

  This will raise an IndexError in the case of an invalid client message, and that needs
  to be caught in the calling function.
  """
  return incoming_message.strdata.split(" ")[1]



def uuid_is_authenticated(uuid: str) -> bool:
  """
  Checks and returns if the given UUID is authenticated to load FW or not.
  """
  return uuid in LOADED_UIOS_PER_UID