import os
import subprocess
import shutil

def find_file_in_subdirs(directory, filename):
    file_path = None
    # Iterate over all subdirectories recursively
    for root, dirs, files in os.walk(directory):
        if filename in files:
          file_path = os.path.join(root, filename)

    return file_path

def find_file_name_pattern_in_subdirs(directory, filename_pattern):
    file_path = None
    # Iterate over all subdirectories recursively
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if filename_pattern in filename:
                file_path = os.path.join(root, filename)

    return file_path
        
def find_dir_in_subdirs(directory, dirname):
    dir_path = None
    # Iterate over all subdirectories recursively
    for root, dirs, files in os.walk(directory):
        if dirname in dirs:
          dir_path = os.path.join(root, dirname)
    
    return dir_path

def find_fw_bitfiles(directory, patterns = [".bit", ".svf", ".dat"]):
  bitfiles = []
  for root, dirs, files in os.walk(directory):
        for file in files:
          for pattern in patterns:
            if pattern in file:
              file_path = os.path.join(root, file)
              bitfiles.append(file_path)

  return bitfiles


def restructure_fw_dir(fw_dir, emp_config):

  # move everything into /fw_dir/tmp
  contents = os.listdir(fw_dir)
  tmp_dir = os.path.join(fw_dir, "tmp")
  os.makedirs(tmp_dir)
  for content in contents:
    src = os.path.join(fw_dir, content)
    if content != 'firmware.tar.gz' and content != "tmp":
      # do not touch the source tarball
      shutil.move(src, tmp_dir)

  # unzip dtbos and address_table
  apollo_package = find_file_in_subdirs(tmp_dir, emp_config["apollo_package_name"])
  proc = subprocess.Popen(["tar", "-xf", apollo_package, "-C", tmp_dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  stdout, stderr = proc.communicate()
  
  # find and move address_table/ and dtbo/ dirs
  address_table_dir = find_dir_in_subdirs(tmp_dir, "address_table")
  shutil.move(address_table_dir, fw_dir)
  dtbo_dir = find_dir_in_subdirs(tmp_dir, "dtbo")
  shutil.move(dtbo_dir, fw_dir)

  # process .bit and .svf files
  bit_dir = os.path.join(fw_dir, "bit")
  os.makedirs(bit_dir)
  bitfiles = find_fw_bitfiles(tmp_dir)
  for bitfile in bitfiles:
    shutil.move(bitfile, bit_dir)

  # process emp addrtab dir
  addrtab_dir = find_dir_in_subdirs(tmp_dir, emp_config["emp_table_dir"])
  shutil.move(addrtab_dir, fw_dir)
  
  # cleanup
  shutil.rmtree(tmp_dir)
