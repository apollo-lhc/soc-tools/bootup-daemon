import os
import logging

from .enums import FPGAProgramStatus
from .helpers_uuid import get_uuid_from_message, uuid_is_authenticated
from .helpers_connection import send_message
from .helpers_repackaging import find_file_name_pattern_in_subdirs

logger = logging.getLogger(__name__)

def program_fpga_for_uuid(
    incoming_message,
    xvc_label,
    sm_instance,
    tarball_target_path
  ) -> FPGAProgramStatus:
  """
  From the incoming message object, figure out the UUID and program the FPGA for it.
  """
  # If the incoming message is valid, it must have the form: "PROGRAM <UID> <FPGA #>"
  # FPGA # can be either 1 or 2
  
  # Figure out the UUID of client from the client message
  try:
    uuid = get_uuid_from_message(incoming_message)
  except IndexError:
    return FPGAProgramStatus.MSG_INVALID

  # Figure out if this UID loaded FW before
  if not uuid_is_authenticated(uuid):
    logger.warning(f"UUID {uuid} does not have FW loaded, ignoring PROGRAM message.")
    return FPGAProgramStatus.NO_REGISTERED_FW

  # Check if CM is powered up
  if sm_instance.ReadRegister("CM.CM_1.CTRL.STATE") != 3:
    logger.error("CM is not powered up, cannot program FPGA.")
    return FPGAProgramStatus.CM_NOT_POWERED_UP

  # Locate the SVF and DAT file for programming
  fw_dir = os.path.join(tarball_target_path, uuid)
  svf_file = find_file_name_pattern_in_subdirs(fw_dir, ".svf")
  dat_file = find_file_name_pattern_in_subdirs(fw_dir, ".dat")
  

  # Could not find an SVF/DAT source file
  if (svf_file is None) and (dat_file is None):
    logger.warning(f"Cannot find SVF or DAT file for FPGA programming under: {fw_dir}")
    return FPGAProgramStatus.SVF_NOT_FOUND

  if dat_file is not None:
    try:
      logger.debug(f"Programming an FPGA with DMA-JTAG.")
      status = sm_instance.progam_fpga(dat_file)
    except Exception as e:
      logger.warning(f"Error while programming FPGA with DMA-JTAG, falling back to the svfplayer: {e}")
      status = sm_instance.svfplayer(svf_file, xvc_label)
  else:
    logger.debug(f"Programming an FPGA with svfplayer.")
    status = sm_instance.svfplayer(svf_file, xvc_label)
  
  if status != 0:
    logger.error("SVFplayer returned non-zero exit code.")
    return FPGAProgramStatus.SVF_PLAYER_FAILED

  logger.debug("Programmed FPGA.")

  # Unblock the AXI bus
  sm_instance.unblockAXI()

  logger.debug("Unblocked AXI bus.")

  return FPGAProgramStatus.OK


def program_fpga_for_uuid_and_send_msg_to_client(
    connection,
    incoming_message,
    outgoing_message,
    xvc_label,
    sm_instance,
    tarball_target_path
  ):
  """
  Program the FPGA and send a message back to client.
  """
  status = program_fpga_for_uuid(incoming_message, xvc_label, sm_instance, tarball_target_path)

  if status == FPGAProgramStatus.OK:
    outgoing_message.strdata = "FPGA is successfully programmed."
  
  elif status == FPGAProgramStatus.MSG_INVALID:
    outgoing_message.strdata = "Invalid message, UUID not specified."

  elif status == FPGAProgramStatus.NO_REGISTERED_FW:
    outgoing_message.strdata = "Not authenticated to load FW, please send a HELLO message first."

  elif status == FPGAProgramStatus.SVF_NOT_FOUND:
    outgoing_message.strdata = "SVF file cannot be found in the tarball, cannot program FPGA."
  
  elif status == FPGAProgramStatus.CM_NOT_POWERED_UP:
    outgoing_message.strdata = "CM is not powered up, cannot program FPGA."
  
  elif status == FPGAProgramStatus.SVF_PLAYER_FAILED:
    outgoing_message.strdata = "SVF player returned non-zero status code."

  # Send the message back to client
  send_message(outgoing_message, connection)
