# 
# Generic helper functions for the UIO daemon.
# 

import os
import shutil
import logging

from .definitions import CONFIG_FS_PATH, CLEANUP_QUEUES, DEVICE_TREE_UUID
from .enums import FWUnloadStatus

logger = logging.getLogger(__name__)

def removeObject(item):
  """
  Remove a symlink, directory or file from the filesystem.
  """
  # Handle symlinks
  if os.path.islink(item):
    os.unlink(item)
  
  # Handle directories
  elif os.path.isdir(item):
    # Check if this is an overlay directory. Since those are under /sys, 
    # shutil.rmtree() call fails due to permission errors. Using os.rmdir() instead works.
    if item.startswith(CONFIG_FS_PATH):
      os.rmdir(item)
    else:
      shutil.rmtree(item)
  
  # Handle files
  else:
    os.remove(item)


def getCleanupQueuesToProcess(uuid):
  """
  For a given client ID, returns a list of cleanup queues that needs to be processed.
  If this is a particular client ID, this can be an array with only one queue. However,
  there are special client ID keywords to pick multiple queues, please see docstring of
  cleanup() below.
  """
  queues_to_process = []
  if uuid not in ['all', 'all-users']:
    # If this client ID is not existent in our server, just ignore 
    # and return an empty list of queues.
    if uuid not in CLEANUP_QUEUES:
      logger.warning(f"UUID {uuid} does not have a cleanup queue, ignoring FW unload request.")
      return []

    queues_to_process.append(CLEANUP_QUEUES[uuid])

  # We'll process every single queue if uuid = 'all' is given
  elif uuid == 'all':
    queues_to_process = CLEANUP_QUEUES.values()

  # If 'all-users' is given we want every queue except the cleanup queue associated with the
  # bootup sequence.
  else:
    for client_id, queue in CLEANUP_QUEUES.items():
      if client_id == DEVICE_TREE_UUID:
        continue
      queues_to_process.append(queue)

  return queues_to_process


def cleanup(uuid='all') -> FWUnloadStatus:
  """
  Cleanup files/directories/symlinks specified in the cleanup queue for the given UUID.

  There are two special cases for the UUID.
  1) uuid = 'all'. This means to cleanup files for every single UUID, including the once loaded on boot.
  2) uuid = 'all-users'. This means to cleanup files for every USER client ID.
  """
  logger.info(f"Starting cleanup for client: {uuid}")
  
  # Figure out which cleanup queue(s) we want to act upon
  queues_to_process = getCleanupQueuesToProcess(uuid)

  # We could not find a cleanup queue for this client ID.
  # So no registered FW to cleanup for this client.
  if not queues_to_process:
    return FWUnloadStatus.NO_REGISTERED_FW

  # Do the cleanup for each queue
  for cleanup_queue in queues_to_process:
    while not cleanup_queue.empty():
      item = cleanup_queue.get()
      if os.path.exists(item):
        logger.info(f"Trying to remove {item}")

        # Try to remove this directory/link/file
        # If we get a PermissionError, log that we failed to remove the item 
        # and move on to the next item
        try:      
          removeObject(item)
          logger.info(f"Removed {item}")
        except PermissionError:
          logger.warning(f"Failed to remove {item}, not enough permissions.")

      else:
        logger.debug("Trying to remove "+item+" as it would be a link")
        if os.path.islink(item):
          logger.info("Removing "+item)
          os.unlink(item)

  return FWUnloadStatus.OK


def removeFWDirectories(dirs_to_remove):
  """
  Removes the list of FW directories given as a list.
  """
  for directory in dirs_to_remove:
    # If the directory is not there, do not try to delete it.
    if not os.path.exists(directory):
      logger.info(f"{directory} is not there, skipping for cleanup.")
      continue
    try:
      logger.info(f"Trying to remove {directory}")
      removeObject(directory)
      logger.info(f"Removed {directory}")
    except BaseException as e:
      logger.error(f"Error while removing {directory}")
      logger.error(str(e))


def cleanupFWDirectoriesContent(dirs_to_remove):
  """
  Navigates inside the dir and cleans up the content without removing the dir itself.
  """
  for directory in dirs_to_remove:
    # If the directory is not there, do not try to delete it.
    if not os.path.exists(directory):
      logger.info(f"{directory} is not there, skipping for cleanup.")
      continue
    try:
      logger.info(f"Trying to clean up contents of {directory}")
      for entry in os.listdir(directory):
          entry_path = os.path.join(directory, entry)
          removeObject(entry_path)
      logger.info(f"Cleaned up contents of {directory}")
    except BaseException as e:
      logger.error(f"Error while cleaning up contents of {directory}")
      logger.error(str(e))
