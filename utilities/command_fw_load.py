import os
import logging
import subprocess
import shutil

from .definitions import CLEANUP_QUEUES, LOADED_UIOS_PER_UID
from .enums import FWLoadStatus

from .helpers_uuid import uuid_is_authenticated, get_uuid_from_message
from .helpers_address_table import write_address_table, write_connections_file, write_emp_connections_file
from .helpers_connection import send_message
from .helpers_overlay import ProcessFWDir
from .helpers_cleanup import cleanup, removeObject
from .helpers_repackaging import find_file_in_subdirs, restructure_fw_dir, find_file_name_pattern_in_subdirs
from .command_fw_unload import unload_fw

logger = logging.getLogger(__name__)

def save_and_extract_fw_tarball(fw_dir, incoming_message, uuid, emp_config):
  """
  From the incoming message, read the tarball content and save it under a new tarball file,
  under the given tarball target path. Finally, extract that new tarball under the same directory.

  If the tar command fails to extract the tarball, automatically clean up the directory
  created for the client under /fw.
  """
  # If this output directory exists, we won't try to override
  if os.path.exists(fw_dir):
    return FWLoadStatus.DIR_EXISTS

  # If the directory is new, proceed
  os.makedirs(fw_dir)

  logger.info(f"FW directory created: {fw_dir}")

  # 1) Save the FW tarball that the server received under this output directory.
  outfile = os.path.join(fw_dir, "firmware.tar.gz")
  with open(outfile, "wb") as f:
    f.write(incoming_message.bytesdata)

  # 2) Now, extract this new tarball
  proc = subprocess.Popen(["tar", "-xf", outfile, "-C", fw_dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  stdout, stderr = proc.communicate()

  # tar command failed
  # If the extraction of the tarball fails, automatically clean up
  if proc.returncode != 0:
    logger.error(f"Extraction failed for tarball: {outfile}")
    cleanup(uuid)

    logger.info(f"Removing client {uuid}")
    LOADED_UIOS_PER_UID.pop(uuid)

    return FWLoadStatus.TAR_FAILED

  # If extraction of the tarball succeeded, all good.
  logger.info(f"Tarball extracted under {fw_dir}")

  # Check if it's EMP structured tarball, look for packaged dtbos and address_table
  apollo_package = find_file_in_subdirs(fw_dir, emp_config["apollo_package_name"])
  if apollo_package is not None:
    logger.info("The tarball was packaged by the EMP fwk, adapting the structure")
    try:
        restructure_fw_dir(fw_dir, emp_config)
    except Exception as e:
        logger.error(f"An error occurred while restructuring the firmware directory: {e}")
        return FWLoadStatus.REPACKAGING_FAILED

  return FWLoadStatus.OK

def get_uuid_for_old_fw(uuid_new, fw_dir_new, devices_config):

  uuid_to_remove = None

  # used to figure out FPGA site (1 or 2)
  f1_ipbus        = devices_config["f1_ipbus"]
  f2_ipbus        = devices_config["f2_ipbus"]
  f1_uio_ipbus    = devices_config["uio_device_prefix"] + f1_ipbus
  f2_uio_ipbus    = devices_config["uio_device_prefix"] + f2_ipbus

  # find uio in a new fw dir
  f1_uio_new_file = find_file_name_pattern_in_subdirs(fw_dir_new, f1_ipbus + ".dtbo")
  f2_uio_new_file = find_file_name_pattern_in_subdirs(fw_dir_new, f2_ipbus + ".dtbo")

  # find which UIDs loaded fw
  uuids = list(LOADED_UIOS_PER_UID.keys())
  # go over them skipping the new one
  for client_id in uuids:
    if client_id != uuid_new:
      uios = LOADED_UIOS_PER_UID[client_id]
      logger.debug(uios)

      # find out if it's F1 or F2 from loaded uio_devices
      if (f1_uio_ipbus in uios) and (f1_uio_new_file is not None):
          uuid_to_remove = client_id
          fpga_site = 'F1'
          logger.info(f"Found uio in the new fw {f1_uio_new_file} matching to the old fw {f1_uio_ipbus} for uuid {client_id}.\n Updating fw for the {fpga_site} site.")
      elif (f2_uio_ipbus in uios) and (f2_uio_new_file is not None):
          uuid_to_remove = client_id
          fpga_site = 'F2'
          logger.info(f"Found uio in the new fw {f2_uio_new_file} matching to the old fw {f2_uio_ipbus} for uuid {client_id}.\n Updating fw for the {fpga_site} site.")
      else:
        logger.info("Target FPGA did not have old fw loaded")

  return uuid_to_remove


def load_fw_for_uuid(
    incoming_message,
    loaded_nodes: dict,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config
  ) -> FWLoadStatus:
  """
  Given the incoming message object, do the following:

  - Check if this UUID is authenticated to load firmware
  - Read the contents of the firmware file and save it under a tarball under the specified target path
  - Load the UIO devices as specified in this firmware.
  """

  # Figure out the UUID of client from the client message
  try:
    uuid = get_uuid_from_message(incoming_message)
  except IndexError:
    return FWLoadStatus.MSG_INVALID

  # Check auth
  if not uuid_is_authenticated(uuid):
    return FWLoadStatus.NO_AUTH

  # 1) Extract the input tarball file and save it under a client-specific FW directory.
  fw_dir = os.path.join(tarball_target_path, uuid)
  # Add to cleanup
  CLEANUP_QUEUES[uuid].put(fw_dir)

  status = save_and_extract_fw_tarball(fw_dir, incoming_message, uuid, emp_config)

  # If the processing of the tarball fails, exit from the function and return the failure status.
  if status != FWLoadStatus.OK:
    return status
  
  emp_connections_file_name = emp_config["emp_connections_file_name"]

  # Find and remove old fw if it was loaded for the FPGA that is being acted upon
  uuid_to_remove = get_uuid_for_old_fw(uuid, fw_dir, devices_config)
  if uuid_to_remove is not None:
    logger.info(f'The new uuid {uuid} is overloading fw for the old uuid {uuid_to_remove}. Removing the loaded fw, address_tables and overlays for the old uuid')
    status_unload_old = unload_fw(
      uuid_to_remove,
      loaded_nodes,
      baMap,
      connections_file_name,
      address_apollo_file_name,
      address_table_path,
      tarball_target_path,
      devices_config,
      emp_config)

  try:
    # 2) Process the firmware directory, load the UIO devices
    loaded_nodes.update(ProcessFWDir(fw_dir, address_table_path, address_apollo_file_name, baMap, devices_config, uuid))
    # 3) Remove the connections_file_name file and the address_apollo_file_name file (in order).
    # We'll rewrite them in the next step with the new list of nodes we have.
    removeObject(os.path.join(address_table_path, connections_file_name))
    removeObject(os.path.join(address_table_path,address_apollo_file_name))
    if os.path.exists(os.path.join(address_table_path, emp_connections_file_name)):
      removeObject(os.path.join(address_table_path, emp_connections_file_name))

    # 4) Write the final address table and the connections file
    write_address_table(os.path.join(address_table_path, address_apollo_file_name), loaded_nodes, baMap)
    write_connections_file(address_table_path, address_apollo_file_name, connections_file_name)
    write_emp_connections_file(address_table_path, tarball_target_path, devices_config, emp_config)
  
  # Handle the case where FW loading failed for this directory,
  # clean up any file that is related to this client ID
  # and return an error state.
  except BaseException:
    cleanup(uuid)
    # Remove this client ID from the list of active IDs
    LOADED_UIOS_PER_UID.pop(uuid)
    return FWLoadStatus.FW_LOAD_FAILED

  # All OK if we reach this state
  return FWLoadStatus.OK


def load_fw_for_uuid_and_send_msg_to_client(
    connection,
    incoming_message,
    outgoing_message,
    loaded_nodes,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config
):
  """
  Load FW for this UUID and send a proper reply back to the client.
  """
  status = load_fw_for_uuid(incoming_message, loaded_nodes, baMap, connections_file_name, address_apollo_file_name, address_table_path, tarball_target_path, devices_config, emp_config)

  if status == FWLoadStatus.OK:
    outgoing_message.strdata = "CM FW is successfully loaded."

  elif status == FWLoadStatus.MSG_INVALID:
    outgoing_message.strdata = "Invalid message, client ID is not specified."

  elif status == FWLoadStatus.NO_AUTH:
    outgoing_message.strdata = "Not authenticated to load FW, please obtain a client ID first."

  elif status == FWLoadStatus.DIR_EXISTS:
    outgoing_message.strdata = "This client has already loaded firmware, please unload that first."

  elif status == FWLoadStatus.TAR_FAILED:
    outgoing_message.strdata = "tar command returned non-zero exit status, untarring failed."

  elif status == FWLoadStatus.FW_LOAD_FAILED:
    outgoing_message.strdata = "Failed to process FW directory and load UIO devices."

  elif status == FWLoadStatus.REPACKAGING_FAILED:
    outgoing_message.strdata = "Failed to repackage a tarball from EMP. Check if the structure corresponds to config."

  # Send the message back to client
  send_message(outgoing_message, connection)
