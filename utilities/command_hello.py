import uuid
import logging

from .definitions import LOADED_UIOS_PER_UID
from .helpers_connection import send_message

logger = logging.getLogger(__name__)

def generate_uuid_and_send_to_client(connection, outgoing_message):
  """
  Helper function to generate a unique user ID and send it back to the client
  using the connection object given.
  """
  # Generate the user ID
  user_id = str(uuid.uuid4())[:8]

  logger.info(f"New client ID generated: {user_id}")

  # Prepare msg object to send back
  outgoing_message.strdata = user_id

  # Send message back to client
  send_message(outgoing_message, connection)

  # Add this uuid to the list of known user IDs, no UIO devices loaded yet
  # so this UID maps to an empty list for now.
  LOADED_UIOS_PER_UID[user_id] = []