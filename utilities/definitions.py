# 
# Important variable definitions.
# 

import queue
from collections import defaultdict

# Important path definitions
SYSTEM_FW_PATH = '/lib/firmware/'                           # Symlink Target Path
CONFIG_FS_PATH = '/sys/kernel/config/device-tree/overlays/' # FW Path
UIO_PATH = '/dev/'                                          # UIO File Path
CONFIG_FILE_PATH = '/etc/uio_daemon.yaml'                   # yaml Configuration File path
PIDFILE_PATH = '/var/run/uio_daemon.pid'                    # PID File Path (Configured in service file)
LOGFILE_PATH = '/var/log/uio_daemon.log'                    # Log File Path


# UUID for the UIO devices being loaded on boot (e.g. UIO devices already in the device-tree)
DEVICE_TREE_UUID = "device-tree"


# Mapping of UID to cleanup queue (queue.LifoQueue), specifying files/dirs to remove while unloading FW.
# When a specific UID wants to unload FW, we'll go through the respective cleanup queue
# and remove items from there.
CLEANUP_QUEUES = defaultdict(queue.LifoQueue)

# Dictionary which maps the UID to the list of loaded UIO devices by that UID.
# - When the client sends "HELLO", we'll add the corresponding UID to the map with an empty list.
# - When the client sends "LOAD <file.tar.gz>", we'll add the UIO files to this UID.
# - When the client sends "UNLOAD", we'll remove the UIO files to this UID.
LOADED_UIOS_PER_UID = {}
