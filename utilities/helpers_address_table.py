#
# Functions related to address table IO.
#

import os
import logging
import xml.etree.ElementTree
import xml.dom.minidom as minidom
from .definitions import LOADED_UIOS_PER_UID
from .helpers_repackaging import find_file_in_subdirs


logger = logging.getLogger(__name__)

def write_address_table(address_table_path, loaded_nodes, baMap):
  """
  Given the list of loaded nodes, update the XML address table under the given path.
  """
  root = xml.etree.ElementTree.Element("node")
  root.attrib['id'] = "Top"
  root.text="\n\t"

  # Check loaded_nodes for succesful loads - None in this dict already had a UIO file
  for node_id in loaded_nodes.keys():
    logger.debug(node_id)
    if "error" in loaded_nodes[node_id].keys():
      if loaded_nodes[node_id]["error"] == "true":
        logger.debug("has error")
        continue

    logger.debug("has no error")
    logger.debug(f'Merged address table: Adding {node_id}')
    new_node = xml.etree.ElementTree.SubElement(root, 'node')
    new_node.attrib['id'] = loaded_nodes[node_id]['id']
    #      new_node.attrib['address'] = "0x"+hex(loaded_nodes[node_id]['address'])[2:].zfill(8)
    new_node.attrib['address'] = str(loaded_nodes[node_id]['address'])
    if not (loaded_nodes[node_id].get('module') is None):
      new_node.attrib['module'] = loaded_nodes[node_id]['module']
    if not (loaded_nodes[node_id].get('fwinfo') is None):
      new_node.attrib['fwinfo'] = loaded_nodes[node_id]['fwinfo']
    new_node.tail = "\n\t"


  # Write output to address table
  xml.etree.ElementTree.ElementTree(root).write(address_table_path)


def parse_xml_file(root, output_path):
  # Create a string representation of the XML
  xml_string = xml.etree.ElementTree.tostring(root, encoding="utf-8")

  # Create a prettified version of the XML string. This is necessary for proper indentation
  dom = minidom.parseString(xml_string)
  pretty_xml_string = dom.toprettyxml(indent="  ", encoding="utf-8")

  # Write the prettified XML to a file
  with open(output_path, "wb") as file:
    file.write(pretty_xml_string)


def write_connections_file(
    address_table_path,
    address_apollo_file_name = "address_apollo.xml",
    file_name="connections.xml"
    ):
  """
  Creates the address table path (if it doesn't exist already), and generates+saves the
  connections.xml file to this directory.
  """
  if not os.path.exists(address_table_path):
    logger.info(f"Creating directory {address_table_path}")
    os.makedirs(address_table_path)

  # Now, generate the connections file.
  output_path = os.path.join(address_table_path, file_name)
  # Create the root element
  root = xml.etree.ElementTree.Element("connections")

  # Create the connection element
  connection = xml.etree.ElementTree.SubElement(root, "connection")
  connection.set("id", "test.0")
  connection.set("uri", "uioaxi-1.0://" + os.path.join(address_table_path, address_apollo_file_name))
  connection.set("address_table", "file://" + os.path.join(address_table_path, address_apollo_file_name))

  # Create the comment element
  comment = xml.etree.ElementTree.Comment("be sure to use the same file in both 'uri' and 'address_table'")
  root.insert(0, comment)

  # save connection file
  parse_xml_file(root, output_path)

def write_emp_connections_file(
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config
    ):
  """
  Creates the address table path (if it doesn't exist already), and generates+saves the
  emp_connections.xml file to this directory for emp fw addrtab. Re-writes a new connectio file for all existing <uuid> that loaded fw.
  <connections>
	<connection id="apollo.c2c1.vu13p" uri="ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" address_table="file:///fw/udm/<uuid>/addrtab/top_emp_apollo.xml" />
	<connection id="apollo.c2c2.vu13p" uri="ipbusmmap-2.0:///dev/uio_F2_IPBUS?offset=0x0" address_table="file:///fw/udm/<uuid>/addrtab/top_emp_apollo.xml" />
  </connections>
  """

  file_name       = emp_config["emp_connections_file_name"]
  emp_table_name  = emp_config["emp_table_name"]
  f1_ipbus        = devices_config["f1_ipbus"]
  f2_ipbus        = devices_config["f2_ipbus"]
  f1_uio_ipbus    = devices_config["uio_device_prefix"] + f1_ipbus
  f2_uio_ipbus    = devices_config["uio_device_prefix"] + f2_ipbus


  if not os.path.exists(address_table_path):
    logger.info(f"Creating directory {address_table_path}")
    os.makedirs(address_table_path)
  # Now, generate the connections file.
  output_path = os.path.join(address_table_path, file_name)
  # Create the root element
  root = xml.etree.ElementTree.Element("connections")

  # find which UIDs loaded fw, should be only 1 or 2!
  uuids = list(LOADED_UIOS_PER_UID.keys())
  for client_id in uuids:
    fw_dir = os.path.join(tarball_target_path, client_id)
    # find emp top .xml file
    emp_top_address = find_file_in_subdirs(fw_dir, emp_table_name)
    if emp_top_address is not None:
      logger.info(f"EMP top address table is under: {emp_top_address}")
      # find out if it's F1 or F2 from loaded uio_devices
      uios = LOADED_UIOS_PER_UID[client_id]
      logger.debug(uios)

      if f1_uio_ipbus in uios:
        # Create the connection element
        connection_F1 = xml.etree.ElementTree.SubElement(root, "connection")
        connection_F1.set("id", f1_ipbus)
        connection_F1.set("uri", f"ipbusmmap-2.0:///dev/{f1_uio_ipbus}?offset=0x0")
        connection_F1.set("address_table", f"file://{emp_top_address}")
      elif f2_uio_ipbus in uios:
        # Create the connection element
        connection_F2 = xml.etree.ElementTree.SubElement(root, "connection")
        connection_F2.set("id", f2_ipbus)
        connection_F2.set("uri", f"ipbusmmap-2.0:///dev/{f2_uio_ipbus}?offset=0x0")
        connection_F2.set("address_table", f"file://{emp_top_address}")
      else:
        logger.info(f"emp_connections file was not written: {f1_uio_ipbus} or {f2_uio_ipbus} do not exist!")



  # save connection file, only if there were emp tarballs
  if len(root) > 0:
    parse_xml_file(root, output_path)
