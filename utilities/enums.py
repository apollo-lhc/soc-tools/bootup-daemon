# 
# Enumerations representing possible states of different operations.
# 

from enum import IntEnum

class FWLoadStatus(IntEnum):
  """
  Enumeration representing different firmware load statuses.
  """
  OK = 0
  DIR_EXISTS = 1         # The firmware directory for the UUID already exists
  MSG_INVALID = 2        # Message sent by client is invalid (e.g. UUID is not specified)
  NO_AUTH = 3            # UUID is not authenticated
  TAR_FAILED = 4         # tar command returned non-zero exit status
  FW_LOAD_FAILED = 5     # Processing of the FW directory failed. Probably a file is missing in the tarball or it's in the wrong location. 
  REPACKAGING_FAILED = 6 # Failed to repackage a tarball from EMP


class FWUnloadStatus(IntEnum):
  """
  Enumeration representing different firmware load statuses.
  """
  OK = 0
  MSG_INVALID = 1          # Message sent by client is invalid (e.g. UUID is not specified)
  NO_REGISTERED_FW = 2     # UUID didn't load FW before


class FPGAProgramStatus(IntEnum):
  """
  Enumeration representing different FPGA programming statuses.
  """
  OK = 0
  MSG_INVALID = 1          # Message sent by client is invalid (e.g. UUID is not specified)
  NO_REGISTERED_FW = 2     # UUID didn't load FW before
  SVF_NOT_FOUND = 3        # SVF file to program FPGA not found under untarred FW directory
  CM_NOT_POWERED_UP = 4    # CM is not powered up yet
  SVF_PLAYER_FAILED = 5    # Programming via svfplayer failed


class CMPowerUpStatus(IntEnum):
  """
  Enumeration representing different CM power up statuses.
  """
  OK = 0
  MSG_INVALID = 1          # Message sent by client is invalid (e.g. UUID is not specified)
  AUTH_FAILED = 2          # UUID is not authenticated to power up the CM
  CMPWRUP_FAILED = 3       # PowerUpCM() call from ApolloSM returned status = False, i.e. failed to power up the CM.
