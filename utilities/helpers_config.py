# 
# Helper functions to parse the YAML configuration file.
# 

import yaml

from .definitions import CONFIG_FILE_PATH

def read_config():
  """
  Reads the configuration file and returns a dictionary. If a field is missing, uses
  the default value.
  """
  # The default configurations
  defaults = {
    "load_on_boot": ["/fw/SM", "/fw/CM/CornellCM_MCU"],
    "address_table_path": "/opt/address_table",
    "plxvc": "PLXVC.XVC_1",
    "cmpwrup_timeout": 5,
  }

  config = {}

  # Read the YAML config file
  with open(CONFIG_FILE_PATH, 'r') as f:
    config_from_yaml = yaml.safe_load(f)

  keys_parsed = []

  for key, val in config_from_yaml.items():
    config[key] = config_from_yaml.get(key, val)
    keys_parsed.append(key)

  # If there are missing keys, set them to their default value
  keys_remaining = list(set(defaults.keys()) - set(keys_parsed))
  for remaining_key in keys_remaining:
    config[remaining_key] = defaults[remaining_key]

  return config
