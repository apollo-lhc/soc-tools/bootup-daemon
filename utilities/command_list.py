# 
# Functions to make lists of UIDs and loaded UIO devices, and send them to the client.
# 

import xml.etree.ElementTree

from .helpers_connection import send_message

from .definitions import LOADED_UIOS_PER_UID


def list_uuids_and_send_msg_to_client(connection, outgoing_message):
  """
  Send a message to the client with the comma separated list of the UUIDs which loaded overlay files.
  """
  # Prepare the message we'll send back to the client
  # We'll send over an XML string with each UUID who loaded FW
  uuids = list(LOADED_UIOS_PER_UID.keys())
  root = xml.etree.ElementTree.Element("clients")
  for client_id in uuids:
    child = xml.etree.ElementTree.SubElement(root, "client")
    child.attrib["uid"] = client_id

  outgoing_message.strdata = xml.etree.ElementTree.tostring(root)

  # Send the message back to client
  send_message(outgoing_message, connection)



def list_uio_devices_and_send_msg_back_to_client(connection, loaded_nodes, outgoing_message):
  """
  Send a message to the client with the list of UIO devices currently installed, 
  together with which UUIDs installed them.
  """
  # Loop over the loaded nodes and construct the string message
  # We'll send over an XML message to the client
  root = xml.etree.ElementTree.Element("devices")
  for node_id, node in loaded_nodes.items():
    child = xml.etree.ElementTree.SubElement(root, "device")
    child.attrib["device_id"] = node_id
    child.attrib["uuid"] = node["uuid"]
    child.attrib["error"] = node["error"]

  outgoing_message.strdata = xml.etree.ElementTree.tostring(root)

  # Send the message back to client
  send_message(outgoing_message, connection)