# 
# Utility functions related to the client-server socket connection.
# 

import socket

def receive(connection, size): 
  """
  Read 'size' number of bytes from the given connection object,
  and return the byte string. If we cannot read any data from the connection,
  returns an empty buffer.
  """
  toread = size
  buf = b''
  # Keep reading until we hit the expected size
  while (toread > 0):
      rsv = connection.recv(toread)

      # If we're not receiving any data (e.g. the client closed the connection), break
      if not rsv:
        break

      buf += rsv
      toread -= len(rsv)
  
  return buf 


def send_message(msg_to_send, connection):
  """
  Helper function to send a message back to client.
  """
  message_to_send = msg_to_send.SerializeToString()
  message_size = msg_to_send.ByteSize()

  # Send the 4-byte header specifying the size of the message
  # and then the message itself
  header = message_size.to_bytes(4, 'little')
  connection.send(header + message_to_send)