import os
import logging

from .definitions import (
  DEVICE_TREE_UUID,
  SYSTEM_FW_PATH,
  CONFIG_FS_PATH,
  CLEANUP_QUEUES,
  LOADED_UIOS_PER_UID,
  UIO_PATH,
)

from .uhal_parser.parsers import simpleParser

from .helpers_cleanup import removeObject

logger = logging.getLogger(__name__)

def overlay_script(dtbo_file_name, dtbo_file_path, uuid=DEVICE_TREE_UUID):
  """
  Function to load the overlay files to /sys/kernel to create the UIO devices.
  """
  logger.debug('Running overlay script')
  # PATHS
  target_path = SYSTEM_FW_PATH + dtbo_file_name + '.dtbo'       # dtbo symlink target path
  fw_path = CONFIG_FS_PATH + dtbo_file_name # dtbo fw path

  # Create symlink if it doesn't already exist
  if (not os.path.exists(target_path)):
    #check if this is a broken symlink
    if os.path.islink(target_path):
      #remove this broken link
      logger.debug(f'Removing broken link: {target_path}')
      removeObject(target_path)

    logger.debug('Creating symlink')
    logger.debug("OG Path: " + dtbo_file_path)
    logger.debug("Target Path: " + target_path)
    os.symlink(dtbo_file_path, target_path)
    
    # Register for cleanup when this UUID wants to unload FW 
    CLEANUP_QUEUES[uuid].put(target_path)
  else:
    logger.debug('Symlink exists')
    #should probably check if this points to the correct file

  # Load FW if it isn't already
  if (not os.path.isdir(fw_path)):
    logger.debug('Loading FW')
    # Create Directory
    os.makedirs(fw_path)
    # Register for cleanup when this UUID wants to unload FW 
    CLEANUP_QUEUES[uuid].put(fw_path)
    # Echo the dtbo filename into the path variable in the new directory
    os.system('echo "' + dtbo_file_name + '.dtbo" > ' + fw_path + '/path')
    logger.info('Loading overlay: '+dtbo_file_name)
  else:
    logger.debug('FW already loaded')



def symlink_modules(src_dir,dest_dir,uuid=DEVICE_TREE_UUID):
  """
  Function to create a symlink of modules_* directory to /opt/address_table.
  """
  #search though all the items in the src_dir folder
  for item in os.listdir(src_dir):    
    if not os.path.isfile(item):
      #this is a dir, check if it is a modules dir
      if item.find("modules_") == 0:
        #this is a modules directory, make a symlink
        dest_path = os.path.join(dest_dir, item)
        src_path = os.path.join(src_dir, item)
        
        # Create the symlink if it doesn't exist already
        if (not os.path.exists(dest_path)):
          os.symlink(src_path,
                     dest_path,
                     target_is_directory=True)
          # Register for cleanup when this UUID wants to unload FW 
          CLEANUP_QUEUES[uuid].put(dest_path)
          logger.debug("Linking "+src_path+" to "+dest_path)
        
        else:
          logger.debug(f"{dest_path} already exists, not trying to override the symlink.")


def ProcessNodeChildren(node,loaded_nodes,baMap,table_dir_path,devices_config, uuid=DEVICE_TREE_UUID):
  #process all of this node's children
  for child in node.getChildren():
    loaded_nodes = ProcessNodeChildren(child,loaded_nodes,baMap,table_dir_path,devices_config,uuid)
  #process this node
  node_id = node.getId()
  if 'uio_endpoint' in node.getFwinfo():
    uio_file_name = devices_config["uio_device_prefix"] + node_id
    address=hex(int(str(baMap.GetBlock(node.span)),0))
    new_node={"node": node, "id": node_id, "address": address,  "fwinfo": node.getFwinfoRaw(), "module": node.getModule(), "uuid": uuid, "error": "false", "span": node.span}

    # Register this UIO device as loaded by this user
    if uuid != DEVICE_TREE_UUID:
      LOADED_UIOS_PER_UID[uuid].append(uio_file_name)

    # Check if the node UIO already exists in /dev/
    if (os.path.exists(UIO_PATH + uio_file_name)):
      logger.info(f"UIO file {uio_file_name} already exists.")
      
      # This UIO device is already loaded, check if it's already on the device-tree upon boot (i.e. loaded by "device-tree" UUID)
      # or loaded earlier by another UUID. In the latter case, we'll completely ignore this entry as two distinct UUIDs cannot
      # load the same UIO device and add to the merged XML address table.
      found_another_node = False

      for loaded_node_id, loaded_node_info in loaded_nodes.items():
        # Found the node loaded by another user, ignore this node
        if (node_id == loaded_node_id) and (loaded_node_info["uuid"] != DEVICE_TREE_UUID):
          found_another_node = True
          logger.warning(f"Node {node_id} already loaded by another UUID, skipping.")
          break

      if not found_another_node:
        loaded_nodes[node_id] = new_node
    
    else:
      # Check if file exists in DTBO directory
      # Ex: /fw/CM/Cornell_rev1_p2_VU7p-1-SM_USP/dtbo/V_IO.dtbo
      dtbo_file_path = table_dir_path + '/dtbo/' + node_id + '.dtbo'
      if (os.path.exists(dtbo_file_path)):
        logger.debug(f'DTBO file found: {dtbo_file_path}')
        # Load file (If it isn't already, handled within overlay_script() function)
        overlay_script(node_id, dtbo_file_path, uuid)
        # Add to loaded nodes
        loaded_nodes[node_id] = new_node
      
      # Failed to find the DTBO file for this device, will not attempt to load
      # Add it to the loaded nodes dictionary, indicating that this node encountered an error
      else:
        logger.info(f'DTBO file {dtbo_file_path} does not exist, will not load this UIO device.')
        new_node["error"] = "true"
        loaded_nodes[node_id] = new_node
    
  return loaded_nodes


def ProcessFWDir(
    table_dir_path,
    dest_dir,
    address_apollo_file_name,
    baMap,
    devices_config,
    uuid=DEVICE_TREE_UUID):
  """
  Process a single firmware directory with address table modules and .dtbo files.  """
  logger.info("Loading " + table_dir_path)

  # Dictionary to store successfully loaded nodes
  loaded_nodes = {}

  if (os.path.exists(table_dir_path)):
    logger.debug(f"Processing directory: {table_dir_path}")
    # Ex: /fw/CM/Cornell_rev1_p2_VU7p-1-SM_USP/address_table/address_Cornell_rev1_p2_VU7p-1-SM_USP.xml
    table_path = table_dir_path + '/' + 'address_table' + '/' + address_apollo_file_name
    try:
      root = simpleParser.ParserNode(name='Root')
      cTree = simpleParser.ParserTree(root)
      cTree.buildTree(root, table_path)    
      loaded_nodes = ProcessNodeChildren(root,loaded_nodes,baMap,table_dir_path, devices_config, uuid)
      #symlink the address table files
      symlink_modules(table_dir_path+"/address_table/",
                      dest_dir, uuid)
    
    # We failed to load the firmware, could be a missing file, or some file not being
    # in the expected path.
    except BaseException as e:
      loaded_nodes["error"] = "Error in " + table_path
      logger.warning("Issue parsing the firmware dir: %s" % (table_dir_path))
      logger.error(e)
      # Re-raise the exception so that the calling function knows we're in error state
      raise e
  
  else:
    logger.debug('Does not Exist')
  return loaded_nodes
