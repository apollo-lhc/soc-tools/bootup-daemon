import os

from .definitions import LOADED_UIOS_PER_UID
from .enums import FWUnloadStatus

from .helpers_connection import send_message
from .helpers_address_table import write_address_table, write_connections_file, write_emp_connections_file
from .helpers_uuid import get_uuid_from_message, uuid_is_authenticated
from .helpers_unload import update_loaded_nodes_for_cleanup
from .helpers_cleanup import cleanup, removeObject


def unload_fw(
    uuid,
    loaded_nodes,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config
) -> FWUnloadStatus:
  """
  Unload FW for the given uuid
  """

  # 3) Figure out which UIO devices we are going to unload, and remove them
  # from the loaded_nodes data structure.
  update_loaded_nodes_for_cleanup(uuid, loaded_nodes,baMap)

  # 4) Remove the emp_connections_file_name file and the address_apollo_file_name file (in order).
  # We'll rewrite them in the next step with the new list of nodes we have.
  removeObject(os.path.join(address_table_path, connections_file_name))
  removeObject(os.path.join(address_table_path, address_apollo_file_name))
  emp_connections_file_name = emp_config["emp_connections_file_name"]
  if os.path.exists(os.path.join(address_table_path, emp_connections_file_name)):
      removeObject(os.path.join(address_table_path, emp_connections_file_name))

  # 5) Write the final address table and the connections file
  write_address_table(os.path.join(address_table_path, address_apollo_file_name), loaded_nodes,baMap)
  write_connections_file(address_table_path, address_apollo_file_name, connections_file_name)


  # 6) Do the full cleanup by removing the UIO devices for this client.
  status = cleanup(uuid)
  if status != FWUnloadStatus.OK:
    return status

  # 7) Finally, remove this client ID from the set of active client IDs. If the special "all-users"
  # client ID is passed, remove every client ID.
  if uuid == "all-users":
    LOADED_UIOS_PER_UID.clear()
  else:
    LOADED_UIOS_PER_UID.pop(uuid, None)

  # 8) Write a new connection file for emp address tables since it needs an updated list of uuids and uio devices
  write_emp_connections_file(address_table_path, tarball_target_path, devices_config, emp_config)

  return FWUnloadStatus.OK

def unload_fw_for_uuid(
    incoming_message,
    loaded_nodes,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config
) -> FWUnloadStatus:
  """
  From the incoming message object, figure out the UUID and unload FW for it.
  """


  # 1) Figure out the UUID of client from the client message
  try:
    uuid = get_uuid_from_message(incoming_message)
  except IndexError:
    return FWUnloadStatus.MSG_INVALID

  # 2) See if this client has already loaded firmware before.
  # If the client didn't load firmware, we cannot unload it.
  # Take note of the special case where the client ID can be "all-users"
  if not uuid_is_authenticated(uuid) and uuid not in ["all-users"]:
    return FWUnloadStatus.NO_REGISTERED_FW

  status = unload_fw(
    uuid,
    loaded_nodes,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config)

  return status


def unload_fw_for_uuid_and_send_msg_to_client(
    connection,
    incoming_message,
    outgoing_message,
    loaded_nodes,
    baMap,
    connections_file_name,
    address_apollo_file_name,
    address_table_path,
    tarball_target_path,
    devices_config,
    emp_config

):
  """
  Unload FW for this UUID and send a proper reply back to the client.
  """
  status = unload_fw_for_uuid(
      incoming_message, 
      loaded_nodes,baMap, 
      connections_file_name, 
      address_apollo_file_name, 
      address_table_path,
      tarball_target_path, 
      devices_config,
      emp_config)

  if status == FWUnloadStatus.OK:
    outgoing_message.strdata = "CM FW is successfully unloaded."

  elif status == FWUnloadStatus.MSG_INVALID:
    outgoing_message.strdata = "Invalid message, client ID is not specified."

  elif status == FWUnloadStatus.NO_REGISTERED_FW:
    outgoing_message.strdata = "The client does not have any registered FW to unload."

  # Send the message back to client
  send_message(outgoing_message, connection)
