#ifndef __UIOINTERFACE_HH__
#define __UIOINTERFACE_HH__

#include <string>
#include <memory>
#include <map>

#include "uio_client.hh"

/*
 * This class provides an interface to the UIOClient functionality,
 * to communicate with the UIO server and do load/unload operations.
 */
class UIOInterface {
    public:
        /* Constructor and destructor. */
        UIOInterface(std::string server_sock);
        ~UIOInterface();

        /* UIO operations using the underlying UIOClient. */
        void LoadFWTarball(std::string tarball_path);
        void UnloadFWTarball();
        void PowerUpCM();
        
        /* 
         * Program FPGA: If a tarball path is passed in, use that tarball to first 
         * load overlays, power up the CM, and then program FPGA.
         */
        void ProgramFPGA();
        void ProgramFPGA(std::string tarball_path);

        /* Obtain lists of client IDs and installed UIO devices. */
        void GetUIODevices(std::map<std::string, std::string>& deviceMap);
        void GetUIDList(std::vector<std::string>& uidList);
    
        /* Returns the client ID used for this interface. */
        std::string GetClientID() { return mUIOClient->GetUID(); }

    private:
        /* Underlying pointer to UIOClient instance for communication with the server. */
        std::shared_ptr<UIOClient> mUIOClient;

        /* Validate underyling UIOClient pointer. Returns false if UIOClient instance
        either doesn't exist, or it does not have a client ID yet. */
        void ValidateUIOClient();

};

#endif 