#ifndef __UIOINTERFACEDEVICE_HH__
#define __UIOINTERFACEDEVICE_HH__

/* Base CommandList class from BUTool. */
#include <BUTool/CommandList.hh>
#include <BUTool/DeviceFactory.hh>
#include <BUTool/VersionTracker.hh>

#include "UIOInterface/UIOInterface.hh"

namespace BUTool {

    class UIOInterfaceDevice : public CommandList<UIOInterfaceDevice> {
        public:
            UIOInterfaceDevice(std::vector<std::string> arg);
            ~UIOInterfaceDevice();
        
        private:
            /* Pointer to UIO interface instance for underlying operations. */
            std::shared_ptr<UIOInterface> mUIOInterface;

            void LoadCommandList();

            /* BUTool CLI command implementations. */
            CommandReturn::status LoadFWTarball(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status UnloadFWTarball(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status PowerUpCM(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status ProgramFPGA(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status GetUIODevices(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status GetUIDList(std::vector<std::string>, std::vector<uint64_t>);
            CommandReturn::status GetCurrentUID(std::vector<std::string>, std::vector<uint64_t>);
    };

}

#endif
