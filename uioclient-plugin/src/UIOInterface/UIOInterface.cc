#include "UIOInterface/UIOInterface.hh"

UIOInterface::UIOInterface(std::string server_sock) : mUIOClient(NULL) {
    /* Initialize the UIOClient pointer. If this fails, an std::runtime_error will
    be raised, which is caught by the calling BUTool SW. */
    mUIOClient = std::make_shared<UIOClient>(server_sock);

    /* Initiate it with a client ID. */
    UIOStatus initStatus = UIOStatus::OK;
    initStatus = mUIOClient->Init();
    
    /* Failed to obtain client ID from the server. */
    if (initStatus != UIOStatus::OK) {
        throw std::runtime_error("Failed to obtain a client ID from the server.\n");
    }
}

/* Destructor. No need to clean up the UIOClient pointer since it's a smart pointer. */
UIOInterface::~UIOInterface() {}

void UIOInterface::ValidateUIOClient() {
    /* Check if the pointer is initialized. */
    if (!mUIOClient) {
        throw std::runtime_error("UIOClient pointer is not initialized.\n");
    }

    /* Check if it has a client ID already. */
    if (mUIOClient->GetUID() == "") {
        throw std::runtime_error("UIOClient instance does not have a valid client ID.\n");
    }
}

/* UIO input/output operations. */
void UIOInterface::LoadFWTarball(std::string tarball_path) {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->SendLoadTarMessage(tarball_path);

    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}

void UIOInterface::UnloadFWTarball() {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->SendUnloadTarMessage();
    
    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}

void UIOInterface::PowerUpCM() {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->SendCMPowerUpMessage();
    
    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}

void UIOInterface::ProgramFPGA() {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->SendProgramFPGAMessage();
    
    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}

void UIOInterface::ProgramFPGA(std::string tarball_path) {
    /* Program the FPGA by first loading the overlays and powering up the CM. */
    LoadFWTarball(tarball_path);
    PowerUpCM();
    ProgramFPGA();
}

void UIOInterface::GetUIODevices(std::map<std::string, std::string>& deviceMap) {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->GetUIODevices(deviceMap);

    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}

void UIOInterface::GetUIDList(std::vector<std::string>& uidList) {
    /* Check if the mUIOClient is initialized properly. */
    ValidateUIOClient();

    UIOStatus status = UIOStatus::OK;
    status = mUIOClient->GetUIDList(uidList);
    
    /* Operation didn't work. */
    if (status != UIOStatus::OK) {
        throw std::runtime_error(mUIOClient->GetServerResponse().c_str());
    }
}
