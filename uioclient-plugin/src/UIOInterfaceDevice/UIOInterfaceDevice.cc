#include <boost/filesystem.hpp>

#include "UIOInterfaceDevice/UIOInterfaceDevice.hh"
#include "UIOInterfaceDevice/Version.hh"

using namespace BUTool;

/* Register UIOInterfaceDevice with BUTool DeviceFactory. */
RegisterDevice(UIOInterfaceDevice,
		 "UIOInterface",    /* Class nickname. */
		 "UIO interface to communicate with UIO daemon running on an ApolloSM.",
		 "u",               /* CLI flag for BUTool. */
		 "uio",             /* Full CLI flag for BUTool. */
		 "Path to the UNIX socket where the UIO server listens.",
		 VersionTracker(nVer,std::string(sVer),std::string(uri))
);

UIOInterfaceDevice::UIOInterfaceDevice(std::vector<std::string> arg) :
    CommandList<UIOInterfaceDevice>("UIOInterface")
{
    /* Set up a UIOInterface instance. */
    mUIOInterface = std::make_shared<UIOInterface>(arg[0]);

    /* Set up commands. */
    LoadCommandList();

}

UIOInterfaceDevice::~UIOInterfaceDevice() {}

/* Command implementations which interface BUTool CLI. */
CommandReturn::status UIOInterfaceDevice::LoadFWTarball(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    /* We need exactly one argument, path to the tarball. */
    if (strArg.size() != 1) {
        return CommandReturn::BAD_ARGS;
    }

    /* Get the tarball path. Check if it is a valid path. */
    std::string tarball_path = strArg[0];
    if ( !boost::filesystem::exists(tarball_path) ) {
        return CommandReturn::BAD_ARGS;
    }

    /* 
     * Pass the tarball to the mUIOInterface instance to load. 
     * Throws std::runtime_error if the operation fails. 
     */
    Print(Level::INFO, "Starting : Load overlays from %s\n", tarball_path.c_str());
    mUIOInterface->LoadFWTarball(tarball_path);
    Print(Level::INFO, "Done     : Load overlays\n");

    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::UnloadFWTarball(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    if (strArg.size() > 0) {
        return CommandReturn::BAD_ARGS;
    }

    Print(Level::INFO, "Starting : Unload overlay files\n");
    mUIOInterface->UnloadFWTarball();
    Print(Level::INFO, "Done     : Unload overlay files\n");
   
    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::PowerUpCM(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    if (strArg.size() > 0) {
        return CommandReturn::BAD_ARGS;
    }

    Print(Level::INFO, "Starting : Power up CM\n");
    mUIOInterface->PowerUpCM();
    Print(Level::INFO, "Done     : Power up CM\n");

    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::ProgramFPGA(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    /* This command can take either a path to a tarball file or no arguments. */
    std::string tarball_path = "";
    switch (strArg.size()) {
        /* Tarball path is not provided. Use .svf file from previously loaded tarball. */
        case 0:
            break;
        /* FW tarball path is explicitly provided. */
        case 1:
            tarball_path = strArg[0];
            break;
        default:
            return CommandReturn::BAD_ARGS;
    }

    /* 
     * If the FW tarball path is not provided to the command, program the FPGA using the .svf 
     * file found in the tarball loaded by this client earlier. 
     */
    if (tarball_path == "") {
        Print(Level::INFO, "Starting : Program FPGA (might take a few minutes)\n");
        mUIOInterface->ProgramFPGA();
        Print(Level::INFO, "Done     : Program FPGA\n");
        return CommandReturn::OK;
    }

    /* 
     * If a FW tarball path is given, first load overlays and power up CM, and then program FPGA.
     * Any of the mUIOInterface calls below will throw an exception if something goes wrong.
     */
    Print(Level::INFO, "Starting : Load overlays from %s\n", tarball_path.c_str());
    mUIOInterface->LoadFWTarball(tarball_path);
    Print(Level::INFO, "Done     : Load overlays\n");
    
    Print(Level::INFO, "Starting : Power up CM\n");
    mUIOInterface->PowerUpCM();
    Print(Level::INFO, "Done     : Power up CM\n");
    
    Print(Level::INFO, "Starting : Program FPGA (might take a few minutes)\n");
    mUIOInterface->ProgramFPGA();
    Print(Level::INFO, "Done     : Program FPGA\n");

    /* If we've come this far without throwing an exception, all good. */
    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::GetUIODevices(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    /* This command takes no arguments. */
    if (strArg.size() > 0) {
        return CommandReturn::BAD_ARGS;
    }
    
    /* This map will contain the mapping between the client ID (who loaded the UIO device) and the device ID. */
    std::map<std::string, std::string> deviceMap;
    mUIOInterface->GetUIODevices(deviceMap);

    /* Print the client-device mapping to the screen. */
    for (auto it = deviceMap.begin(); it != deviceMap.end(); it++) {
        Print(Level::INFO, "%-20s %s\n", it->first.c_str(), it->second.c_str());
    }

    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::GetUIDList(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    /* This command takes no arguments. */
    if (strArg.size() > 0) {
        return CommandReturn::BAD_ARGS;
    }

    /* Array of client IDs to be filled. */
    std::vector<std::string> clientIDArray;
    mUIOInterface->GetUIDList(clientIDArray);

    /* Print the client IDs to the screen. */
    if (clientIDArray.size() == 0) {
        Print(Level::INFO, "No clients have loaded FW.\n");
        return CommandReturn::OK;
    }

    for (auto it = clientIDArray.begin(); it != clientIDArray.end(); it++) {
        Print(Level::INFO, "%s\n", (*it).c_str());
    }
    
    return CommandReturn::OK;
}

CommandReturn::status UIOInterfaceDevice::GetCurrentUID(std::vector<std::string> strArg, std::vector<uint64_t> intArg) {
    /* This command takes no arguments. */
    if (strArg.size() > 0) {
        return CommandReturn::BAD_ARGS;
    }

    /* Retrieve the client ID for the current session. */
    std::string clientId = mUIOInterface->GetClientID();
    Print(Level::INFO, "Client ID: %s\n", clientId.c_str());
    return CommandReturn::OK;
}

/* Function to set up BUTool CLI commands. */
void UIOInterfaceDevice::LoadCommandList() {
    AddCommand("uio-loadtar", &UIOInterfaceDevice::LoadFWTarball,
        "Load overlay files present in the given tarball.\n" \
        "Usage: \n" \
        "    uio-loadtar /path/to/overlays.tar.gz \n"
    );

    AddCommand("uio-unloadtar", &UIOInterfaceDevice::UnloadFWTarball,
        "Unload previously loaded overlay files by this client.\n" \
        "You will not need to pass a client ID to this command, \n" \
        "as it will automatically use the ID of associated client.\n" \
        "Usage: \n" \
        "    uio-unloadtar\n"
    );

    AddCommand("cmpowerup", &UIOInterfaceDevice::PowerUpCM,
        "Power up the Command Module.\n" \
        "Usage: \n" \
        "   cmpowerup\n"
    );

    AddCommand("programfpga", &UIOInterfaceDevice::ProgramFPGA,
        "Program one FPGA on the CM.\n" \
        "Usage: \n" \
        "    programfpga \n" \
        "    programfpga /path/to/firmware.tar.gz \n" \
        "\n" \
        "If no tarball path is given, an SVF file is used from an existing tarball loaded\n" \
        "by the same client earlier. If a tarball is provided, the overlays will be loaded first,\n" \
        "and the FPGA will be programmed.\n"
    );

    AddCommand("getuiodevices", &UIOInterfaceDevice::GetUIODevices,
        "Prints a mapping between loaded UIO devices and the client IDs who loaded them.\n" \
        "Usage: \n" \
        "    getuiodevices\n"
    );

    AddCommand("getclientids", &UIOInterfaceDevice::GetUIDList,
        "Prints the list of client IDs who loaded firmware to the screen.\n" \
        "Usage: \n" \
        "    getclientids\n"
    );

    AddCommand("getcurrentid", &UIOInterfaceDevice::GetCurrentUID,
        "Get the client ID for the current session.\n" \
        "Usage: \n" \
        "   getcurrentid \n"
    );
}