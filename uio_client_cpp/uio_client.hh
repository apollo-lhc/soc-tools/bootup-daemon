/*
 * UIO client written in C++ to talk to the UIO daemon.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <stdint.h>
#include <ctime>
#include <memory>    // std::shared_ptr
#include <algorithm> // std::find
#include <vector>    // std::vector
#include <map>       // std::map

// For XML string parsing
#include <pugixml.hpp>

// Include the header file defining the UIOMessage object
#include "uio_message.pb.h"

// For Python types
#pragma GCC diagnostic push
//poorly written code in pybind11 (turn off pedantic warnings)
#pragma GCC diagnostic ignored "-Wpedantic"
#include "pybind11/stl.h"
#pragma GCC diagnostic pop

namespace py = pybind11;

/*
 * Status enum for UIO operations.
 */
enum class UIOStatus {
    OK,
    FILE_NOT_FOUND,      /* Tarball file (for overlays) not found. */
    UID_NOT_SET,         /* Client UID is not set. */
    SOCK_WRITE_FAILED,   /* Writing to the socket failed. */
    SOCK_READ_FAILED,    /* Reading from the socket failed. */
    SERVER_ERROR,        /* Server could not finish the operation. */
    XML_PARSER_ERROR,    /* Failed to parse the XML string sent by the server. */
};

/*
 * Status enum for socket I/O operations.
 */
enum class SocketIOStatus {
    READ_OK,
    READ_FAILED,
    WRITE_OK,
    WRITE_FAILED,
};

/*
 * Messages to expect from the server.
 */
#define FW_LOAD_SUCCESS_MSG      "CM FW is successfully loaded."
#define FW_UNLOAD_SUCCESS_MSG    "CM FW is successfully unloaded."
#define CMPWRUP_SUCCESS_MSG      "Powered up the CM."
#define PROGRAM_FPGA_SUCCESS_MSG "FPGA is successfully programmed."

/*
 * Helper class for socket I/O operations.
 */
class SocketIOHelper {
    public:
        SocketIOHelper(std::string socket_path);

        /* Functions to write to, or read from a socket with the given file descriptor. */
        SocketIOStatus WriteToSock(std::string strdata, std::string bytesdata="");
        SocketIOStatus ReadFromSock(uio_message::UIOMessage& message);

    private:
        /* Receive len bytes from the socket using recv() call from UNIX. */
        int Receive(void *buf, size_t len, int flags);

        /* Socket file descriptor to communicate with the server. */
        int sockfd;
        /* Information about the server */
        struct sockaddr_un server;

};

/*
 * UIO client class which defines functionality to send messages to the server
 * in terms of protocol buffers, and capture the reply back.
 */
class UIOClient {
    public:
        /* The constructor handles the creation of the socket file descriptor
        to communicate with the server. */
        UIOClient(std::string socket_path);

        /* 
         * Initiation functions. 
         * If this is called without a UID argument, it will ask the server for one. 
         * It is also possible to call it with a UID to set the internal UID member.
         */
        UIOStatus Init();
        UIOStatus Init(std::string _uid);

        /* Load/unload overlays by providing the path to the tarball. */
        UIOStatus SendLoadTarMessage(std::string tarball_path);
        UIOStatus SendUnloadTarMessage();

        /* CM power up + program FPGA commands. */
        UIOStatus SendCMPowerUpMessage();
        UIOStatus SendProgramFPGAMessage();

        /* List UIDs and UIO devices. */
        UIOStatus GetUIODevices(std::map<std::string, std::string>& deviceMap);
        UIOStatus GetUIDList(std::vector<std::string>& uidList);

        /* 
         * Overloaded methods of above. These functions allow to take Python lists/dicts
         * as parameters and modify them in place. These are only meant to be useful for
         * Python binding applications.
         */
        UIOStatus GetUIODevices(py::dict& deviceMap);
        UIOStatus GetUIDList(py::list& uidList);

        /* Get the UID for this client, which is used for auth on server side. */
        std::string GetUID() { return uid; }

        /* Get the server response for the latest transaction with this client. */
        std::string GetServerResponse() { return serverResponse; }

    private:
        /* 
         * Helper function to send a message to the server, and read the response back. 
         * The response object will be stored in the server_response object passed to the function by reference.
         */
        UIOStatus SendMessageAndReadBack(std::string strdata, std::string bytesdata, uio_message::UIOMessage &server_response);

        /* Helper which implements socket IO */
        std::shared_ptr<SocketIOHelper> iohelper; 

        /* 
         * Client unique ID for authentication.
         * This member must be set before sending any messages to the API that
         * require authentication (e.g. program FGPA). 
         */
        std::string uid;

        /*
         * The string data sent back by the server, indicating the
         * status of the operation. 
         */
        std::string serverResponse;
};
