/*
 * UIO client written in C++ to talk to the UIO daemon.
 */

#include "uio_client.hh"


SocketIOHelper::SocketIOHelper(std::string socket_path) {
    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);

    // Failed to open the socket
    if (sockfd < 0) {
        throw std::runtime_error("Failed to open the socket.\n");
    }

    // Listening server is a UNIX socket
    server.sun_family = AF_UNIX;

    // Set the socket path for the server
    strcpy(server.sun_path, socket_path.c_str());

    // Try to connect to the server
    if (connect(sockfd, (struct sockaddr *) &server, sizeof(struct sockaddr_un)) < 0) {
        close(sockfd);
        throw std::runtime_error("Failed to connect to the server.\n");
    }
}

int SocketIOHelper::Receive(void *buf, size_t len, int flags) {
    size_t toread = len;
    char  *bufptr = (char*) buf;

    while (toread > 0) {
        ssize_t rsz = recv(sockfd, bufptr, toread, flags);
        if (rsz <= 0)
            return rsz;  /* Error or other end closed connection */

        toread -= rsz;  /* Read less next time */
        bufptr += rsz;  /* Next buffer position to read into */
    }

    return len;
} 

SocketIOStatus SocketIOHelper::WriteToSock(std::string strdata, std::string bytesdata) {
    // Prepare the message which we will send
    uio_message::UIOMessage message;    
    uint32_t message_size;

    message.set_strdata(strdata);

    /* If we're sending a file stream, include it in the message. */
    if (bytesdata != "") {
        message.set_bytesdata(bytesdata);
    }

    std::string message_string;
    message.SerializeToString(&message_string);
    message_size = message.ByteSizeLong(); 

    // Construct the buffer to send to the server
    size_t header_size = sizeof(uint32_t);
    char* buf = new char[header_size + message_size];

    // Insert the header (message_size) and the message itself to the buffer
    for (size_t i=0; i < header_size; i++) {
        buf[i] = (message_size >> 8*i) & 0xFF;
    }

    for (size_t i=0; i < message_size; i++) {
        buf[header_size+i] = message_string[i];
    }

    /* Write buffer to the server socket. Number of bytes we're going to write 
    is the sum of message and header sizes. */
    if (write(sockfd, buf, message_size + header_size) < 0) {
        /* Failed to write to the socket. */
        delete[] buf;
        return SocketIOStatus::WRITE_FAILED;
    }

    /* Cleanup and return. */
    delete[] buf;

    return SocketIOStatus::WRITE_OK;
}

SocketIOStatus SocketIOHelper::ReadFromSock(uio_message::UIOMessage& message) {
    int rvalheader, rvalmsg;
    std::string buffer_string;

    // First, read the header
    size_t headersize = 4;
    char* header = new char[headersize];

    rvalheader = Receive(header, headersize, 0);
    
    // Check for error cases
    if (rvalheader < 0) {
        delete[] header;
        return SocketIOStatus::READ_FAILED;
    }

    // If all OK, proceed to read the rest of the message
    uint32_t bufsize = ((uint32_t*)header)[0];
    char* buffer = new char[bufsize];
    rvalmsg = Receive(buffer, bufsize, 0);
    
    // Check for error cases
    if (rvalmsg < 0) {
        delete[] header;
        delete[] buffer;
        
        return SocketIOStatus::READ_FAILED;
    }

    // Construct a C++ string from the buffer
    buffer_string.assign(buffer, rvalmsg);

    // Parse this C++ string using protocol buffer library
    // After this step, the string data sent by the server can be accessed via message.strdata()
    message.ParseFromString(buffer_string);

    /* Cleanup and return. */
    delete[] header;
    delete[] buffer;

    return SocketIOStatus::READ_OK;
}


UIOClient::UIOClient(std::string socket_path) : 
    uid(""), serverResponse("") {
    /* Create a pointer to the IOHelper class, which handles socket I/O operations. */
    iohelper = std::make_shared<SocketIOHelper>(socket_path);

}

UIOStatus UIOClient::SendMessageAndReadBack(std::string strdata, std::string bytesdata, uio_message::UIOMessage &server_response) {
    /* Write to the socket and check status. */
    SocketIOStatus write_status = iohelper->WriteToSock(strdata, bytesdata);
    if (write_status != SocketIOStatus::WRITE_OK) {
        return UIOStatus::SOCK_WRITE_FAILED;
    }
    /* If all good, now read back from the server. The resulting data 
    will be saved under the server_response object. */
    SocketIOStatus read_status = iohelper->ReadFromSock(server_response);

    /* Check if read is OK. */
    if (read_status != SocketIOStatus::READ_OK) {
        return UIOStatus::SOCK_READ_FAILED;
    }

    /* If the read is OK, set the serverResponse attribute
    with the string data came in from the server. */
    serverResponse = server_response.strdata();

    return UIOStatus::OK;
}

UIOStatus UIOClient::Init() {
    /* Send a "HELLO" message to the server to obtain a UID. */
    std::string strdata = "HELLO";

    /* Send a "HELLO" message to the server and read the response back. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("HELLO", "", server_response);

    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* Obtain the UID from the response. */
    uid = server_response.strdata();
    return UIOStatus::OK;     

}

UIOStatus UIOClient::Init(std::string _uid) {
    uid = _uid;
    return UIOStatus::OK;
}

UIOStatus UIOClient::SendLoadTarMessage(std::string tarball_path) {
    /* Check if this client has a valid UID. */
    if (GetUID() == "") {
        return UIOStatus::UID_NOT_SET;
    }

    /* String message we're going to send to the server. */
    std::string strdata = "LOAD " + GetUID();
    /* File data we're going to send (i.e. the tarball). */
    std::string bytesdata;

    std::ifstream stream(tarball_path.c_str(), std::ios::binary);
    /* File does not exist. */
    if (!stream.good()) {
        return UIOStatus::FILE_NOT_FOUND;
    }

    // Get size of file
    stream.seekg(0, std::ios::end);
    int size = (int) stream.tellg();
    stream.seekg(0, std::ios::beg);
    // Read into buf, assign to string
    char* bytebuf = new char[size];
    stream.read(bytebuf, size);

    /* Update bytesdata. */
    bytesdata.assign(bytebuf, size);
    stream.close();
    delete[] bytebuf;

    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack(strdata, bytesdata, server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* Operation failed, server error. */
    if (server_response.strdata() != FW_LOAD_SUCCESS_MSG) {
        return UIOStatus::SERVER_ERROR;
    }
    return UIOStatus::OK;
}

UIOStatus UIOClient::SendUnloadTarMessage() {
    /* Check if this client has a valid UID. */
    if (GetUID() == "") {
        return UIOStatus::UID_NOT_SET;
    }
    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("UNLOAD " + GetUID(), "", server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* Operation failed, server error. */
    if (server_response.strdata() != FW_UNLOAD_SUCCESS_MSG) {
        return UIOStatus::SERVER_ERROR;
    }
    return UIOStatus::OK;
}

UIOStatus UIOClient::SendCMPowerUpMessage() {
    /* Check if this client has a valid UID. */
    if (GetUID() == "") {
        return UIOStatus::UID_NOT_SET;
    }

    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("CMPWRUP " + GetUID(), "", server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }
    
    /* Operation failed, server error. */
    if (server_response.strdata() != CMPWRUP_SUCCESS_MSG) {
        return UIOStatus::SERVER_ERROR;
    }
    return UIOStatus::OK;
}

UIOStatus UIOClient::SendProgramFPGAMessage() {
    /* Check if this client has a valid UID. */
    if (GetUID() == "") {
        return UIOStatus::UID_NOT_SET;
    }
    
    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("PROGRAM " + GetUID(), "", server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* Operation failed, server error. */
    if (server_response.strdata() != PROGRAM_FPGA_SUCCESS_MSG) {
        return UIOStatus::SERVER_ERROR;
    }
    return UIOStatus::OK;
}

UIOStatus UIOClient::GetUIODevices(std::map<std::string, std::string>& deviceMap) {
    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("DEVICES", "", server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* 
     * Server will respond with an XML string with the list of UIO devices.
     * Parse that and store the {device : user} mapping in the passed deviceMap object.
     */
    std::string devicesXml = server_response.strdata();

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_string(devicesXml.c_str());

    /* Failed to parse the XML. */
    if (!result) {
        return UIOStatus::XML_PARSER_ERROR;
    }

    pugi::xml_node devicesNode = doc.child("devices");
    for (pugi::xml_node deviceNode : devicesNode.children("device")) {
        std::string deviceID = deviceNode.attribute("device_id").as_string();
        std::string uuid = deviceNode.attribute("uuid").as_string();

        deviceMap[deviceID] = uuid;
    }    
    return UIOStatus::OK;
}

UIOStatus UIOClient::GetUIDList(std::vector<std::string>& uidList) {
    /* Write to the server socket and read back the response. */
    uio_message::UIOMessage server_response;
    UIOStatus status = SendMessageAndReadBack("UIDS", "", server_response);
    /* Check if operation is OK. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /*
     * Server will respond with an XML string with each client UID
     * who loaded overlays. Parse that and fill the uidList.
     */
    std::string clientsXml = server_response.strdata();

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_string(clientsXml.c_str());

    /* Failed to parse the XML. */
    if (!result) {
        return UIOStatus::XML_PARSER_ERROR;
    }

    pugi::xml_node clientsNode = doc.child("clients");
    for (pugi::xml_node clientNode : clientsNode.children("client")) {
        std::string clientId = clientNode.attribute("uid").as_string();
        uidList.push_back(clientId);
    }

    return UIOStatus::OK;

}

UIOStatus UIOClient::GetUIODevices(py::dict& deviceMap) {
    /* 
     * C++ map for UIO devices. We'll save the information on this C++ object,
     * and write the data to the Python dictionary being passed in.
     */
    std::map<std::string, std::string> cppDeviceMap;
    UIOStatus status;
    status = GetUIODevices(cppDeviceMap);

    /* GetUIODevices() failed. */
    if (status != UIOStatus::OK) {
        return status;
    }

    /* Clear and write to the Python dictionary */
    deviceMap.clear();
    for (const auto& item : cppDeviceMap) {
        deviceMap[py::str(item.first)] = item.second;
    }
    
    return UIOStatus::OK;
}

UIOStatus UIOClient::GetUIDList(py::list& uidList) {
    /*
     * C++ array for UID list. Save the list of UIDs to this object and
     * write the data to the Python list being passed in.
     */
    std::vector<std::string> cppUIDList;
    UIOStatus status;
    status = GetUIDList(cppUIDList);

    /* GetUIDList() failed. */
    if (status != UIOStatus::OK) {
        return status;
    }
    
    /* Write to the Python list */
    for (const auto& uid : cppUIDList) {
        uidList.append(uid);
    }

    return UIOStatus::OK;
}
