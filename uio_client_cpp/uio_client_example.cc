/*
 * Example C++ code for using the UIOClient object to:
 *
 * - Load overlays
 * - Power up an Apollo CM and program an FPGA
 * - Unload overlays
 * 
 * You can use this code with a single command line argument, which is
 * the path to the tarball file containing the overlay (.dtbo) files and
 * the firmware (.svf) file. 
 */

#include <iostream>

#include "uio_client.hh"


void print_error_message(UIOStatus status) {
    switch (status)
    {
    case UIOStatus::FILE_NOT_FOUND:
        std::cout << "Tarball file not found." << std::endl;
        break;
    case UIOStatus::UID_NOT_SET:
        std::cout << "Client does not have a valid UID." << std::endl;
        break;
    case UIOStatus::SOCK_WRITE_FAILED:
        std::cout << "Writing to the server socket failed." << std::endl;
        break;
    case UIOStatus::SOCK_READ_FAILED:
        std::cout << "Reading from the server socket failed." << std::endl;
        break;
    case UIOStatus::SERVER_ERROR:
        std::cout << "UIO server failed to finish the request." << std::endl;
        break;
    case UIOStatus::XML_PARSER_ERROR:
        std::cout << "Failed to parse the XML string sent by the server." << std::endl;
        break;
    default:
        std::cout << "Unknown status." << std::endl;
        break;
    }
}


int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cout << "Wrong number of arguments." << std::endl;
        std::cout << "Usage: ./uio_client_example /path/to/fw_tarball.tar.gz" << std::endl;
        return -1;
    }

    /* Path to the UNIX socket where server is listening. */
    std::string socket_path = "/var/run/uio_daemon/uio_socket";

    /* Path to the tarball with FW and overlay files. */
    std::string tarball_path = argv[1];

    UIOStatus status;

    UIOClient client = UIOClient(socket_path);

    /* Step 1: Get UID from server. */
    status = client.Init();
    if (status != UIOStatus::OK) {
        std::cout << "Failed to obtain a UID for the client" << std::endl;
        return -1;
    }

    std::cout << "Client ID obtained from server: 0x" << client.GetUID() << std::endl;
    
    /* Step 2: Load overlays and program an FPGA. */
    std::cout << "Loading overlays" << std::endl;
    status = client.SendLoadTarMessage(tarball_path);
    if (status != UIOStatus::OK) {
        print_error_message(status);
        return -1;
    }
    
    std::cout << "Powering up the CM" << std::endl;
    status = client.SendCMPowerUpMessage();
    if (status != UIOStatus::OK) {
        print_error_message(status);
        return -1;
    }
    
    std::cout << "Programming the FPGA" << std::endl;
    status = client.SendProgramFPGAMessage();
    if (status != UIOStatus::OK) {
        print_error_message(status);
        return -1;
    }

    std::cout << "Unloading overlays and exiting" << std::endl;
    status = client.SendUnloadTarMessage();
    if (status != UIOStatus::OK) {
        print_error_message(status);
        return -1;
    }

    return 0;
}

