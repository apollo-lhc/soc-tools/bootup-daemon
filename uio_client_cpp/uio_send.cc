/*
 * Program to send messages to the UIO API server using the UIOClient object.
 */

#include <iostream>
#include <boost/program_options.hpp>

#include "uio_client.hh"

namespace po = boost::program_options;

/*
 * These API directives require the user to pass a UID via the command line.
 */
std::vector<std::string> DIRECTIVES_NEED_UID = {
    "unload-tar",
};

/*
 * These API directives require the user to pass a tarball file via the command line.
 */
std::vector<std::string> DIRECTIVES_NEED_FILE = {"load-tar"};

/*
 * List of all known directives.
 */
std::vector<std::string> ALL_DIRECTIVES = {
    "get-uid",
    "load-tar",
    "unload-tar",
    "program-fpga",
    "cmpwrup",
    "list-devices",
    "list-uids"
};

int init_client(UIOClient &client, std::string uid="") {
    /* Obtain a UID for the client. If we already have one, just use that. */
    UIOStatus status;
    
    /* UID already provided. */
    if (uid != "") {
        status = client.Init(uid);
        std::cout << "Using provided client ID: 0x" << client.GetUID() << std::endl;
    }
    /* UID not provided, obtain one from the server. */
    else {
        std::cout << "Client ID not provided, obtaining from server." << std::endl;
        status = client.Init();
    }

    /* Check if Init() call succeeded. */
    if (status != UIOStatus::OK) {
        std::cout << "Failed to obtain client ID from server." << std::endl;
        return -1;
    }

    /* If all good, print out the client ID and return. */
    std::cout << "Client ID: 0x" << client.GetUID() << std::endl;

    return 0;
}

bool validate_cli_args(std::string directive, std::string filepath, std::string uid) {
    /*
     * Helper function to validate CLI arguments passed to this program.
     */

    /* Check if a client ID is passed in. */
    auto it = std::find(DIRECTIVES_NEED_UID.begin(), DIRECTIVES_NEED_UID.end(), directive);
    if ((it != DIRECTIVES_NEED_UID.end()) && (uid == "")) {
        std::cout << "Please pass an existing UID (via -u) for directive " << directive << std::endl;
        return false;
    }

    /* Check if a file has been passed. If not, we cannot send this directive to the API server. */
    if (std::find(DIRECTIVES_NEED_FILE.begin(), DIRECTIVES_NEED_FILE.end(), directive) != DIRECTIVES_NEED_FILE.end()) {
        if (filepath == "") {
            std::cout << "Please pass path to the tarball file via -f for directive " << directive << std::endl;
            return false;
        }
    }

    /* For program-fpga, either a tarball or a client ID must be passed in. */
    if (directive == "program-fgpa") {
        if ((filepath == "") && (uid == "")) {
            std::cout << "Please either pass a tarball file with -f or a client ID with -u." << std::endl;
            return false;
        }

        if ((filepath != "") && (uid != "")) {
            std::cout << "Please either pass a tarball with -f OR a client ID with -u. Both are not supported at the same time." << std::endl;
            return false;
        }

    }
    /* All OK at this point. */
    return true;
}


int main(int argc, char *argv[]) {
    /*
     * Verify that the version of the library that we linked against is
     * compatible with the version of the headers we compiled against.
     */
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    /* Parse command line options. */
    po::options_description desc("CLI options");
    desc.add_options()
        ("help,h", "Display help message and exit.")
        ("directive,d", po::value<std::string>(), "The API directive to send.")
        ("uid,u",       po::value<std::string>()->default_value(""), "The UID to use for server auth.")
        ("filepath,f",  po::value<std::string>()->default_value(""), "Path to the tarball with overlay files.");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (const po::error& ex) {
        std::cerr << "Error during command line parsing: " << ex.what() << std::endl;
        return -1;
    }

    /* Display help message and exit. */
    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    /* An API directive must be set. */
    if (!vm.count("directive")) {
        std::cout << "An API directive must be set via --directive or -d." << std::endl;
        return -1;
    }

    std::string directive = vm["directive"].as<std::string>();
    std::string uid = vm["uid"].as<std::string>();
    std::string filepath = vm["filepath"].as<std::string>();

    /* Validate command line arguments. */
    if (!validate_cli_args(directive, filepath, uid)) {
        return -1;
    }

    /* Initiate the client. */
    UIOClient client = UIOClient("/var/run/uio_daemon/uio_socket");

    int init_status = 0;
    UIOStatus status = UIOStatus::OK;

    /* 
     * Send the command. If a command requires a valid client ID (i.e. for auth)
     * use init_client() call to get a valid UID first.
     */
    if (directive == "get-uid") {
        /* Get a client ID from the server. If everything goes fine,
        resulting ID will be printed to the terminal. */
        init_status = init_client(client);
        if (init_status != 0) { return -1; }
    }

    else if (directive == "load-tar") {
        init_status = init_client(client, uid);
        if (init_status != 0) { return -1; }
        
        status = client.SendLoadTarMessage(filepath);
    }
    else if (directive == "unload-tar") {
        init_status = init_client(client, uid);
        if (init_status != 0) { return -1; }

        status = client.SendUnloadTarMessage();
    }
    else if (directive == "cmpwrup") {
        init_status = init_client(client, uid);
        if (init_status != 0) { return -1; }

        status = client.SendCMPowerUpMessage();
    }
    else if (directive == "program-fpga") {
        init_status = init_client(client, uid);
        if (init_status != 0) { return -1; }

        /* If a tarball file is passed in with this directive, load the overlays first. */
        if (filepath != "") {
            status = client.SendLoadTarMessage(filepath);
            /* Operation failed. */
            if (status != UIOStatus::OK) {
                std::cout << "Failed to load overlays from " << filepath << std::endl;
                return -1;
            }

            status = client.SendCMPowerUpMessage();
            if (status != UIOStatus::OK) {
                std::cout << "Failed to power up the CM." << std::endl;
                return -1;
            }

            /* Finally, program the FPGA. */
            status = client.SendProgramFPGAMessage();

        }
        /* If a client ID is passed in instead, directly program the FPGA for that client. */
        else {
            status = client.SendProgramFPGAMessage();
        }
    }
    else if (directive == "list-devices") {
        std::map<std::string, std::string> deviceMap;
        status = client.GetUIODevices(deviceMap);

        /* Print out deviceMap, which has -> { deviceID : UID } */
        for (const auto & pair : deviceMap) {
            printf("%-20s %s\n", pair.first.c_str(), pair.second.c_str());
        }
    }
    else if (directive == "list-uids") {
        std::vector<std::string> uidList;
        status = client.GetUIDList(uidList);

        /* Print out the list of UIDs. */
        for (const std::string & client_id : uidList) {
            printf("0x%s\n", client_id.c_str());
        }

        /* If the list of UIDs is empty, print that. */
        if (uidList.size() == 0) {
            printf("No client IDs found that loaded firmware.\n");
        }

    }
    /* Unknown directive specified. Show a help message and exit. */
    else {
        std::cout << "Unknown API directive specified. Options are: ";
        for (const std::string & directive : ALL_DIRECTIVES) {
            std::cout << directive << ", ";
        }
        std::cout << std::endl;
        return -1;
    }

    /* Check status */
    if (status != UIOStatus::OK) {
        std::cout << "Operation failed: " << directive << std::endl;
        return -1;
    }

    return 0;
}

