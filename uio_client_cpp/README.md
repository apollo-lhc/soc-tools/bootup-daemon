# C++ UIO Client

This directory includes the source code for the `UIOClient` class written in C++. It also includes
a helper client implementation `uio_send.cc`, which can be used to send specific directives to the UIO
API (for instructions, see below).
To build the `uio_send` binary, which uses the `UIOClient` object to communicate with the server, you can just type `make`.

This software requires the following libraries to be installed in the system:

- pugixml (for XML parsing)
- protobuf (protocol buffer runtime library)
- pthread

## Using uio_send

When `uio_send` is built, the executable can be used to send messages to the UIO API server. The API directives are the same with the ones defined in `../uio_send.py`. Namely, one can execute:

```bash
./uio_send -d '<API directive>' -u '<UUID (optional)>' -f '<FW file path (optional)>'
```

If a UUID (used for authentication by the server) is not provided, a new one will be asked from the server. The API directives are shown below:

```bash
# Get client ID (UUID)
./uio_send -d get-uid

# Load overlays: 
# With a previously provided client ID
./uio_send -d load-tar -u <UUID> -f /path/to/firmware.tar.gz

# Ask the server for a client ID on the fly
./uio_send -d load-tar -f /path/to/firmware.tar.gz

# Unload overlays
./uio_send -d unload-tar -u <UUID>

# Power up CM
./uio_send -d cmpwrup -u <UUID>

# Program an FPGA on the CM:
# First option: Using a previously loaded FW tarball by the same UUID
./uio_send -d program-fpga -u <UUID>

# Second option: Pass in a tarball, load overlays from that tarball and program the FPGA
./uio_send -d program-fpga -f /path/to/firmware.tar.gz

# List devices and/or UUIDs who loaded FW
./uio_send -d list-devices
./uio_send -d list-uids
```
