# bootup-daemon

## Purpose
A systemd service daemon that ensures nodes listed in specified address tables are loaded properly and UIO files are generated before allowing dependent services to execute.

## Installation

### Prerequisites
- Python Packages
    - [lxml](https://pypi.org/project/lxml/)*
    - [jinja2]
    - [python-pidfile](https://pypi.org/project/python-pidfile/)
    - [python-daemon](https://pypi.org/project/python-daemon/)
- Yum Packages
    - libxml2
    - libxml2-devel
    - libxml2-python
    - libxslt
    - libxslt-devel

Note: The yum packages are dependencies of lxml. The [official lxml documentation](https://lxml.de/installation.html) utilizes apt-get to install the packages. Since the blades are RedHat based, yum is used instead.

### File Setup
- Copy `uio_daemon.yaml` config file into `/etc/`
- Copy `uio_daemon.service` into `/opt/BUTool/systemd`
- Create a symlink of the service file: `ln -s /opt/BUTool/systemd/uio_daemon.service /etc/systemd/system`

### Service Setup
- `sudo systemctl daemon-reload`
- `sudo systemctl enable uio_daemon.service`
- `sudo systemctl start uio_daemon.service`


## The UIO Server

The `uio_daemon.py` sets up an API which listens for messages in the form of Google protocol buffers. The structure and attributes of the message are defined in `protobuf_src/msg.proto` file. Using the protocol buffer compiler, this file needs to be compiled into a Python file, which defines the message class used. Please see the `README` under `protobuf_src` for instructions about this.

By default, the API will listen in a UNIX socket located on the blade in `/var/run/uio_daemon/uio_socket`.
The server listens for following messages:

- `HELLO`: Obtain a unique ID (UID) to get authentication for loading/unloading overlays, and programming the FPGA.
- `LOAD '<client ID>'`: Load overlay files. A tarball must be passed with this command (which has the `.dtbo` files for the overlays) to the server. In addition, a valid UID must also be passed for authentication.
- `UNLOAD '<client ID>'`: Unload overlay files for the given UID.
- `CMPWRUP '<client ID>'`: Power up the Command Module.
- `PROGRAM '<client ID>'`: Program the FPGA. This command **must be called after** the `LOAD` and `CMPWRUP` commands, and the tarball passed during the `LOAD` stage will be used to locate the `.svf` file to use for programming.
- `DEVICES`: List the currently installed UIO devices on blade OS. Server will send an XML string to the client specifying the devices and which UID installed them.
- `UIDS`: List the UIDs which currently have installed overlays.

## UIO Client Operations

For communication with the server, `uio_send.py` script is provided, which can be used to send messages interactively to the server. Below, the usage of `uio_send.py` is summarized.

**Note:** All the `uio_*.py` scripts are typically located under `/opt/uio-daemon/python` in an Apollo Service Module filesystem. This directory is automatically included in the `$PATH` environment variable,
so from any directory, you should be able to simply execute `uio_send.py`. You can check the `$PATH` and if necessary, add the UIO software path manually:

```bash
# Check the paths for executables
echo $PATH

# If /opt/uio-daemon/python is not there, you can add it manually for ease of use
PATH=/opt/uio-daemon/python:$PATH
```

### Getting a Client ID

A client ID can be obtained from the server using the `get-uid` directive
as follows:

```bash
uio_send.py get-uid
```

The obtained client ID will be printed to the terminal, which then can be used for other operations such as `load-tar`, `program-fgpa` and so on.

### Loading Overlays

Use the `load-tar` directive to load overlays in a tarball. The tarball should be passed in with
the`-f` flag (see below). It is possible to execute this directive in
two ways:

* Without specifying a client ID, in this case a new ID will be obtained from the server.
* With specifying a client ID, so that this client ID will be passed on to the server.

Both methods are shown below:

```bash
# Do not specify a client ID: One will be obtained from the server on the fly
uio_send.py load-tar -f /path/to/fw/tarball.tar.gz

# Explicitly specify a client ID
uio_send.py load-tar -f /path/to/fw/tarball.tar.gz -u '<client ID>'
```

You can check if the overlays are loaded by utilizing the `list-uids` and `list-devices` directives:

```bash
# List the UIO devices installed
uio_send.py list-devices

# List the UIDs who installed overlay files
uio_send.py list-uids
```

### Unloading Overlays

Use the `unload-tar` directive with the client UID:

```bash
uio_send.py unload-tar -u '<client ID>'
```

### Programming the FPGA

Use the `cmpwrup` and `program-fpga` directives with the client UID.

There are two ways to use the `program-fpga` directive, either by passing an existing client ID with `-u` or passing a tarball file with `-f`:

* If a client ID is passed in via `-u`, the server will locate the tarball loaded by this client, and use the SVF file from there to program the FPGA.
* If a tarball file is passed with `-f`, the server will assign the client a new ID, load the overlays from the tarball, and then power up the CM and program the FPGA.

Note that the following are invalid operations:

* Passing a client ID and tarball file **at the same time**.
* Passing **neither** a client ID or a tarball file.

Both options to execute are shown below:

```bash
# First option: Using an existing client ID, power up the CM and program the FPGA.
uio_send.py cmpwrup -u '<client ID>'
uio_send.py program-fpga -u '<client ID>'

# Second option: Do it all in one go by passing a file:
uio_send.py program-fpga -f /path/to/firmware.tar.gz

# These are invalid!
uio_send.py program-fpga
uio_send.py program-fpga -u '<client ID>' -f /path/to/firmware.tar.gz
```
## Programming the FPGA with FW packaged by the EMP framework (for CMS users only)

FW tarballs built by the EMP framework contain both apollo-specific address tables and overlays packaged in `apollo_package.tgz`, and the set of EMP address tables with a bit file combined together. User can use any of the commands shown above, for ex:

```bash
uio_send.py program-fpga -f /path/to/firmware.tar.gz
```

If `top_emp.xml` is found in the tarball, it will be treated as "EMP-like" fw. When that tarball is processed, the daemon creates two connections files: for the apollo address tables, `connections.xml`, and for the EMP-fwk FW, `emp_connections.xml`. All these names and variables can be changed in `uio_daemon.yaml` config. 


## Using UIOClient from BUTool CLI

This repository also provides a BUTool plugin called `uioclient-plugin`, allowing to communicate with the UIO server directly from the BUTool command line interface.
This is achieved by loading the library for the *UIOInterfaceDevice* to BUTool runtime and adding the device as follows:

```bash
# Launch BUTool.exe with an ApolloSM
$ BUTool.exe -a

# Load the device library (if not loaded already)
> add_lib /opt/uio-daemon/lib/libUIOInterfaceDevice.so

# Add the device itself, with a path to the socket where the UIO server listens for incoming requests
> add_device UIOINTERFACE /var/run/uio_daemon/uio_socket
```

As soon as the `add_device` call has been made to create a *UIOInterfaceDevice* instance, this interface will ask the server for a unique client ID. Therefore, you will not need
to obtain one yourself. You can retrieve the obtained client ID by:

```bash
# Get the automatically obtained client ID
> getcurrentid
```

You can also obtain the full list of client IDs and loaded UIO devices:

```bash
# Get a list of client IDs who loaded firmware/overlays
> getclientids

# Get a list of installed UIO devices
> getuiodevices
```

### Load Overlays & Program FPGA

You can load overlay files, power up the CM and program an FPGA in one go using the `programfpga` command (this might take a few minutes):

```bash
> programfpga /path/to/tarball.tar.gz
```

Alternatively, you can do the steps above separately:

```bash
# Load overlay files
> uio-loadtar /path/to/tarball.tar.gz

# Power up the CM
> cmpowerup

# Program the FPGA, this uses the .svf file located in the tarball loaded via uio-loadtar
> programfpga
```

### Unload Overlays

Overlays can also be unloaded by simply executing:

```bash
> uio-unloadtar
```

## Building and Packaging the Software

The UIO client and server software can be built and then be packaged as an RPM.

### Build

To build the software, simply execute `make` from the command line. This will build:

* UIO message source files for C++ and Python3
* UIOClient software
* Python3 bindings for the UIOClient

### Package with RPM

To package this software as an RPM, you can do the following:

```bash
cd rpm/
make
```

The RPMs will include the Python3 source files, C++ headers, and the shared library for the `UIOClient` class, together with the Python bindings for it.

In addition to the "full" daemon rpm, a "lightweight" version is built - `UIODaemonPlugin`, which can be used for building software with the `UIOClient` class. For instance, as a plugin in a container.

Please note that RPMs for multiple architectures are built by GitLab CI jobs, and resulting RPM files can be installed from the job artifacts.

## Files

### uio_daemon.py
Primary daemon script.

Utilizes [python-daemon](https://pypi.org/project/python-daemon/) DaemonContext instance to be configured properly and run as a Unix daemon process.

Responsible for reading configuration file and handling each specified node properly depending on respective node status quo.

The script begins by parsing the configuration file and checking each node. If the node is already loaded properly, it is skipped, otherwise it is loaded.

The loading process is handled by the overlay_script() function that checks if the firmware has already been loaded, and if not, creates the firmware directory and echoes the into it, from which the UIO files are auto-generated.

If a node is successfully loaded, it is added to the new address table file which is generated by this script. The new address table is a copy of the old address table so that previously loaded nodes are also included and newly added nodes are appended.

Once the script finishes handling each node from each address table specified in the configuration file, it defines the signal handler and opens a listener before entering an infinite while loop with a sleep function.

Once the signal handler trips, it executes the cleanup() function that goes through each successfully loaded node and deletes all files and directories created throughout the loading process. This includes:
- DTBO file symlinks
- Firmware directories
- New address table


### uio_daemon.service
[systemd unit configuration file](https://www.freedesktop.org/software/systemd/man/systemd.service.html)

The `Unit` section contains the `Before` option where all the services that should run after this one are specified.

The `Service` section contains the `ExecStart` option that specifies the installed Python version to be used when executing the desired Python script, in this case uio_daemon.py

The `KillSignal` option is also set to `SIGINT` so that that signal handler in uio_daemon.py can listen for it and cleanly exit once it is received.

Path: `/opt/BUTool/systemd`

### `uio_daemon.yaml` config file
Configuration file in which the path of address tables containing the nodes to be loaded can be specified.

The file also contains the path of the existing address table (called the old_table) that specifies already loaded nodes. This file is copied in uio_daemon.py and newly loaded nodes are appended. The name and path of this copy are specified in this file as well (new_table)

Path: `/etc/uio_daemon.yaml`

### uio_daemon.log - /etc/uio_daemon
Generated by uio_daemon.py using the [Python logging module](https://docs.python.org/3/howto/logging.html)

Path: `/var/log/uio_daemon.log`
