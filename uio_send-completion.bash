#/usr/bin/env bash
## Registers bash completions for uio_send.py executable
## The file must be installed to /usr/share/bash-completion/completions/
complete -W "get-uid load-tar unload-tar list-uids list-devices program-fpga cmpwrup" uio_send.py

function _uio_send_completions() {
	latest="${COMP_WORDS[$COMP_CWORD]}"
	prev="${COMP_WORDS[$COMP_CWORD - 1]}"
	words=""
	case "${prev}" in
		uio_send.py)
			words="get-uid load-tar unload-tar list-uids list-devices program-fpga cmpwrup"
			COMPREPLY=($(compgen -W "$words" -- $latest))
			;;
		-u)
			# UIDs can be found in this directory instead of calling list-uids
			# Redirect stderr to /dev/null in case directory doesn't exist
			words=`ls /fw/udm/ 2>/dev/null`
			COMPREPLY=($(compgen -W "$words" -- $latest))
			;;
		-f)
			COMPREPLY=($(compgen -f -d -- $latest))
			;;
	esac
	return 0
}		
complete -F _uio_send_completions uio_send.py
