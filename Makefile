default: build

UIO_CLIENT_CPP_PATH=$(abspath ./uio_client_cpp)
UIO_CLIENT_PLUGIN_PATH=$(abspath ./uioclient-plugin)
PROTOBUF_SRC_PATH=$(abspath ./protobuf_src)
RPM_PATH=$(abspath ./rpm)

# Update submodules
init:
	git submodule update --init --recursive

# Build UIOClient software
build:
	$(MAKE) -C ${PROTOBUF_SRC_PATH} build
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} build
	$(MAKE) -C ${UIO_CLIENT_PLUGIN_PATH} build

# Clean all build artifacts
clean:
	$(MAKE) -C ${PROTOBUF_SRC_PATH} clean
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} clean
	$(MAKE) -C ${UIO_CLIENT_PLUGIN_PATH} clean
	$(MAKE) -C ${RPM_PATH} clean

install:
	$(MAKE) -C ${UIO_CLIENT_CPP_PATH} install
	$(MAKE) -C ${UIO_CLIENT_PLUGIN_PATH} install