#!/usr/bin/env python3

#
# Python3 script to send messages to the UIO server running as a daemon on an Apollo blade.
# Can send HELLO, LOAD, UNLOAD, UIDS, DEVICES and PROGRAM messages to the UIO server.
#

import os
import sys
import argparse

# 
# Import the UIOClient module using the Python bindings.
# 
try:
    from UIOClient import UIOClient, UIOStatus
except ModuleNotFoundError:
    print("UIOClient module not found. Are the Python bindings installed?")
    sys.exit(1)

# 
# List of different API directives that can be passed to this script (see parse_cli() below).
# 
MESSAGE_CHOICES = [
    "get-uid",
    "load-tar",
    "unload-tar",
    "list-uids",
    "list-devices",
    "program-fpga",
    "cmpwrup",
]

# 
# These directives require a tarball file to be provided with them.
# 
MESSAGES_WITH_FILE = ["load-tar"]

# 
# These directives require a valid UID to be passed in with them.
# 
MESSAGES_WITH_UID = [
    "unload-tar", 
    "cmpwrup",
]

def validate_cli_args(args):
    """
    Validate command line arguments.
    """
    # Is a tarball file passed in?
    if args.directive in MESSAGES_WITH_FILE and args.file_path == "":
        print("Please also specify the file to load via -f option.")
        return -1

    # Is a client ID passed in?
    if args.directive in MESSAGES_WITH_UID and args.uuid == "":
        print("Please also specify a client ID for this operation via -u or --uuid.")
        return -1

    # program-fpga needs either a file or a client ID (but not both!)
    if args.directive == "program-fpga": 
        # Neither file or client ID are passed in, at least one should be present.
        if (not args.uuid and not args.file_path):
            print("Please either specify a tarball file with -f, or a client ID with -u.")
            return -1
        
        # Both client ID and file path passed in, this is not valid either.
        if (args.uuid and args.file_path):
            print("Please either pass a tarball with -f OR a client ID with -u. Both are not supported at the same time.")
            return -1
    
    # All OK at this point
    return 0


def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("directive", choices=MESSAGE_CHOICES, help="Type of message to send to server.")
    parser.add_argument('-f', '--file-path', default="", help="Path to the tarball to load.")
    parser.add_argument('-sp', '--socket-path', default="/var/run/uio_daemon/uio_socket", help="Path to the UNIX socket for the UIO server.")
    parser.add_argument('-u', '--uuid', default="", help="Client ID for the operation.")
    args = parser.parse_args()

    # Command line argument checks
    if validate_cli_args(args) < 0:
        sys.exit(1)
    
    return args


def init_client(client, client_id=""):
    """
    Initiate the UIOClient with a client ID. If the passed client ID is NULL 
    (i.e. an empty string), client will be initiated with an ID from the server.

    Otherwise, the client will be initiated with the given ID.
    """
    # Initiate the client with a pre-defined client ID
    if client_id != "":
        status = client.Init(client_id)
    
    # If the client ID is not given beforehand, ask one from the server
    else:
        print("Obtaining a client ID from the server.")
        status = client.Init()

        # Terminate if we fail to obtain client ID from the server
        if status != UIOStatus.OK:
            print("Failed to obtain a client ID from the server.")
            return -1
    
    print(f"Client ID: {client.GetUID()}")
    return 0

def main():
    args = parse_cli()

    # Instantiate the client
    client = UIOClient(args.socket_path)

    # Obtain a client ID from the server
    # The client ID will be printed to the terminal assumming everything went fine.
    if args.directive == "get-uid":
        init_client(client)

    # List installed UIO devices
    elif args.directive == "list-devices":
        deviceMap = {}
        status = client.GetUIODevices(deviceMap)
        
        # Operation failed
        if status != UIOStatus.OK:
            print(f"Error during {args.directive} directive: {status}")
            return

        # If all OK, print out the devices
        for device_id, uuid in deviceMap.items():
            print(f"{device_id:<20} {uuid:<20}")

    # List UUIDs which loaded overlays
    elif args.directive == "list-uids":
        client_ids = []
        status = client.GetUIDList(client_ids)

        # Operation failed
        if status != UIOStatus.OK:
            print(f"Error during {args.directive} directive: {status}")
            return

        # If all OK, print out the UIDs
        for client_id in client_ids:
            print(client_id)

        # If no client IDs found, print a message
        if len(client_ids) == 0:
            print("No client IDs found that loaded firmware.")

    # Load overlay files
    elif args.directive == "load-tar":
        if not os.path.exists(args.file_path):
            print(f"Tarball file does not exist: {args.file_path}, exiting.")
            return
        
        # Initiate client
        if init_client(client, args.uuid) < 0:
            return

        status = client.SendLoadTarMessage(args.file_path)
        # Operation failed
        if status != UIOStatus.OK:
            print(f"Error during {args.directive} directive: {status}")
            print(f"Response from server: {client.GetServerResponse()}")
            return

    # Unload overlay files
    elif args.directive == "unload-tar":
        # Initiate client
        if init_client(client, args.uuid) < 0:
            return
        
        status = client.SendUnloadTarMessage()
        # Operation failed
        if status != UIOStatus.OK:
            print(f"Error during {args.directive} directive: {status}")
            print(f"Response from server: {client.GetServerResponse()}")
            return

    # Program the FPGA
    elif args.directive == 'program-fpga':
        # Initiate client
        if init_client(client, args.uuid) < 0:
            return
        
        # If a tarball file is passed in, we will first load the overlay files
        # and then program the FPGA.
        if args.file_path:
            status = client.SendLoadTarMessage(args.file_path)
            # Check status of operation
            if status != UIOStatus.OK:
                print(f"Error during loading overlays from {args.file_path}")
                print(f"Response from server: {client.GetServerResponse()}")
                return

            # If all OK, power up the CM
            status = client.SendCMPowerUpMessage()
            # Check status of operation
            if status != UIOStatus.OK:
                print(f"Error during powering up CM, status: {status}")
                print(f"Response from server: {client.GetServerResponse()}")
                return

            # If all OK, now program the FPGA
            status = client.SendProgramFPGAMessage()
            # Check status
            if status != UIOStatus.OK:
                print(f"Error during programming FPGA, status: {status}")
                print(f"Response from server: {client.GetServerResponse()}")
                return

        # If a tarball file is not passed in, we have a client ID. So that
        # we can locate an earlier tarball loaded with this client ID and 
        # program the FPGA using that tarball. 
        else:
            status = client.SendProgramFPGAMessage()
            # Operation failed
            if status != UIOStatus.OK:
                print(f"Error during {args.directive} directive: {status}")
                print(f"Response from server: {client.GetServerResponse()}")
                return

    # Power up the CM
    elif args.directive == "cmpwrup":
        # Initiate client
        if init_client(client, args.uuid) < 0:
            return
        
        status = client.SendCMPowerUpMessage()
        # Operation failed
        if status != UIOStatus.OK:
            print(f"Error during {args.directive} directive: {status}")
            print(f"Response from server: {client.GetServerResponse()}")
            return

    # Print the OK server response, unless the command was not to list devices/users.
    if args.directive not in ['list-uids', 'list-devices']:
        print(f"Response from server: {client.GetServerResponse()}")


if __name__ == "__main__":
    main()
