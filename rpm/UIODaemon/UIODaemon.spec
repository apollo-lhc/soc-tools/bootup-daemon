#
# Spec file for UIO daemon RPMs.
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: UIODaemon
License: Apache License
Group: SoC-Tools
Source: https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon
URL: https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
UIO client and server software to load DTBO overlay files and FPGA firmware.

%prep

# Ensure Python3.6
%global __python %{__python36}

%build

%install 

# Copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
mkdir -p $RPM_BUILD_ROOT%{_py_prefix}
mkdir -p $RPM_BUILD_ROOT%{_pybind_prefix}
mkdir -p $RPM_BUILD_ROOT%{_completions_prefix}

cp -rp %{cpp_sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.
cp -rp %{py_sources_dir}/* $RPM_BUILD_ROOT%{_py_prefix}/.
cp -rp %{pybind_sources_dir}/* $RPM_BUILD_ROOT%{_pybind_prefix}/.
cp -p  %{systemd_service_file_dir}/uio*service $RPM_BUILD_ROOT%{_prefix}/.
cp -p  %{completions_script_dir}/*bash $RPM_BUILD_ROOT%{_completions_prefix}/uio_send.py

# Ensure Python 3 shebangs
find $RPM_BUILD_ROOT -name "*.py" -exec sed -i '1s|^#!/usr/bin/python$|#!/usr/bin/python3|' {} \;

# Copy daemon config file
mkdir -p $RPM_BUILD_ROOT/etc
cp -p %{specs_dir}/uio_daemon.yaml $RPM_BUILD_ROOT/etc/uio_daemon.yaml

# Change access rights, make some files executable
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/bin
chmod 755 $RPM_BUILD_ROOT%{_py_prefix}/uio_send.py

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_completions_prefix}/uio_send.py
%{_prefix}/uio_daemon.service
%{_prefix}/lib/*
%{_prefix}/bin/*
%{_prefix}/include/*
%{_py_prefix}/*
%{_pybind_prefix}/*cpython*.so
/etc/uio_daemon.yaml
