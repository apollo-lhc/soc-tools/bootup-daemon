#
# Spec file for UIO daemon plugin RPMs.
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: UIODaemon
License: Apache License
Group: SoC-Tools
Source: https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon
URL: https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
A plugin to talk to the UIODaemon:
UIO client and server software to load DTBO overlay files and FPGA firmware.

%prep

%build

%install 

# Copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{cpp_sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.


# Change access rights, make some files executable
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/bin

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_prefix}/lib/*
%{_prefix}/bin/*
%{_prefix}/include/*

